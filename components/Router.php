<?php

class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath); //подключаем маршруты
    }

    /**
     * getURI
     * @return string
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * run
     * Запуск маршрутизатора
     */
    public function run()
    {
        //получаем строку запроса
        $uri = $this->getURI();

        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                $internalRoute  = preg_replace("~$uriPattern~", $path, $uri);

                $base           = explode('/', $path);
                $internalRoute  = stristr($internalRoute, $base[0]);

                $segments       = explode('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                //array_shift получает первую строку из массива и удаляет ее из массива

                $controllerName = ucfirst($controllerName);
                $actionName     = 'action' . ucfirst(array_shift($segments));
                $parameters     = $segments;

                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                } else {
                    echo 'Контроллер ' . $controllerName . ' не найден!';
                    break;
                }
                $controllerObject = new $controllerName;
                if (method_exists($controllerObject, $actionName)) {
                    $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                    // call_user_func_array - вызывает функцию и передает ей параметр
                    if ($result != null) {
                        break;
                    } else {
                        break;
                    }
                } else {
                    echo 'Вызов не существующего метода!';
                    break;
                }
            }
        }
    }
}