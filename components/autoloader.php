<?php

spl_autoload_register(function ($class) {
    if (file_exists("models/{$class}.php")) {
        require_once "models/{$class}.php";
    } elseif (file_exists('models/' . strtolower($class) . '.php')) {
        require_once 'models/' . strtolower($class) . '.php';
    } elseif (file_exists('models/DB/' . strtolower($class) . '.php')) {
        require_once 'models/DB/' . strtolower($class) . '.php';
    } elseif (file_exists('controllers/' . strtolower($class) . '.php')) {
        require_once 'controllers/'.strtolower($class).'.php';
    }else {
        print "Файл models/{$class}.php, или controllers/{$class}.php не найден";
    }
});