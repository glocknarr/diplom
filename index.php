<?php

session_start();

require_once('config/params.php'); // Подключаем константы
require_once(ROOT . '/components/Router.php'); // Маршрутизатор
require_once(ROOT . '/components/autoloader.php'); // Автолоадер
include     (ROOT. '/components/simple-php-captcha.php'); // Каптча

$db = new DBModel();
$_SESSION['captcha']    = simple_php_captcha();
$router                 = new Router;
$router -> run();
ob_start();
header("Location: index");
ob_end_clean();

/*
 * Для загрузки картинок и аватарок в файле php.ini должна быть включена строка
 * file_uploads=On
 *
 * Проверяется с помощью функции phpinfo(), далее поиском по слову upload
 */