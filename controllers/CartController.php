<?php

class CartController
{
    public function actionIndex($id)
    {
        $id             = (int)$id;
        $productInfo    = Cart::buyProduct($id);

        if (isset($_POST['submit'])){
            $dataList = [];

            if (isset($_POST['product_Id']) && !empty($_POST['product_Id'])) {
                $dataList['product_Id'] = $_POST['product_Id'];
            } else {
                $dataList['product_Id'] = false;
            }
            if (isset($_POST['customer_Id']) && !empty($_POST['customer_Id'])) {
                $dataList['customer_Id'] = $_POST['customer_Id'];
            } else {
                $dataList['customerId'] = false;
            }
            if (isset($_POST['order_name']) && !empty($_POST['order_name'])) {
                $dataList['order_name'] = $_POST['order_name'];
            } else {
                $dataList['order_name'] = false;
            }
            if (isset($_POST['order_surname'])) {
                $dataList['order_surname'] = $_POST['order_surname'];
            } else {
                $dataList['order_surname'] = '';
            }
            if (isset($_POST['order_patronymic'])) {
                $dataList['order_patronymic'] = $_POST['order_patronymic'];
            } else {
                $dataList['order_patronymic'] = '';
            }
            if (isset($_POST['order_email']) && !empty($_POST['order_email'])) {
                $dataList['order_email'] = $_POST['order_email'];
            } else {
                $dataList['order_email'] = false;
            }
            if (isset($_POST['order_phone'])) {
                $dataList['order_phone'] = $_POST['order_phone'];
            } else {
                $dataList['order_phone'] = '';
            }
            if (isset($_POST['address']) && !empty($_POST['address'])) {
                $dataList['address'] = $_POST['address'];
            } else {
                $dataList['order_address'] = false;
            }
            if (isset($_POST['order_description'])) {
                $dataList['order_description'] = $_POST['order_description'];
            } else {
                $dataList['order_description'] = '';
            }
            if (!Orders::buyProduct($dataList)) {
                // Если запись заказа не удалась, выводить ошибку
                echo '<pre>';
                print_r("Ошибка создания заказа");
                echo '</pre>';
                die;
            }
            header('Location: index');
           
        } else {
            require_once(ROOT . '/view/cart/index.php');
        }
        return true;
    }
}