<?php

class CatalogController
{
    public function actionCategory($categoryId, $page = 1)
    {
        if((int)$page <= 0){
            $page = 1;
        }
        $categories         = ProductCategory::getProductCategoryList();
        $productCategories  = Product::getProductsListByCategory($categoryId, $page);
        $total              = Product::countProducts($categoryId);
        $pagination         = new Pagination($total, PRODUCTS_ON_PAGE, $page, 'page-');
        require_once(ROOT . '/view/catalog/category.php');
    }
}