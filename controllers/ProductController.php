<?php

class ProductController
{
    private $tmp_file_name;    // Имя временного файла картинки товара

    /**
     * actionView
     * Просмотр страницы товара
     * @param $id
     * @return array|bool|mixed|null
     */
    public function actionView($id)
    {
        $categories = ProductCategory::getProductCategoryList();
        $product    = Product::getProductById($id);
        $shopName   = Shop::getShopName($id);

        require_once ROOT . '/view/product/view.php';
        return true;
    }


    /**
     * actionEdit
     * Редактирование товара
     * @param $idProduct
     * @return bool
     */
    public function actionEdit($idProduct)
    {
        $userId             = User::checkLogged();
        $access             = User::getUserAccess($userId);
        $user               = User::getUserById($userId);
        $product            = Product::getProductById($idProduct);
        $productCategory    = ProductCategory::getProductCategoryList();
        if ($product['picture'] == ''){
            $product['picture'] = DEFAULT_PICTURES;
        }

        if (isset($_SESSION['tmp_picture']) && $_SESSION['tmp_picture'] != ''){
            $this->tmp_file_name = $_SESSION['tmp_picture'];
        }
        $tmp_pict = $this->tmp_file_name;

        if (isset($_POST['submit'])) {
            $tmp_pict = '';
            $name           = $_POST['name'];
            $price          = intval($_POST['price']);
            $category       = $_POST['category'];
            $payment_info   = $_POST['payment_info'];
            $delivery_info  = $_POST['delivery_info'];
            $description    = $_POST['description'];

            if (isset($_SESSION['tmp_picture']) && $_SESSION['tmp_picture'] != ''){
                $this->tmp_file_name = $_SESSION['tmp_picture'];
            }
            $this->tmp_file_name != ''  ? $fileName = $this->tmp_file_name  : $fileName = '';
            if (isset($_POST['is_new'])) {
                $is_new = 1;
            } else {
                $is_new = 0;
            }
            if (isset($_POST['for_sale'])) {
                $for_sale = 1;
            } else {
                $for_sale = 0;
            }
            if (isset($_POST['visible'])) {
                $visible = 1;
            } else {
                $visible = 0;
            }
            if ($access['name'] != 'admin') {
                $shopId = $_SESSION['shop'];
            } else {
                $shopId = $product['shop_id'];
            }

            $newProduct = array(
                'shop_id'       => $shopId,
                'product'       => $idProduct,
                'name'          => $name,
                'price'         => $price,
                'category'      => $category,
                'payment_info'  => $payment_info,
                'delivery_info' => $delivery_info,
                'description'   => $description,
                'is_new'        => $is_new,
                'for_sale'      => $for_sale,
                'visible'       => $visible,
                'picture'       => $fileName
            );

            if ($access['name'] != 'admin'){
                $isValid = Product::checkProduct($idProduct); //Товар принадлежит пользователю?
            } else {
                $isValid = true;
            }
            if ($isValid) {
                Product ::editProduct($newProduct);
                if ($fileName != ''){
                    // Если временный файл существует, удаляем его
                    if (file_exists(USERFILES . '/tmp/' . $fileName)){
                        unlink(USERFILES . '/tmp/' . $fileName);
                    }
                    $fileName               = '';
                    $this->tmp_file_name    = '';
                    $_SESSION['tmp_picture']= '';
                }
            }
            if ($access['name'] == 'admin'){
                header('Location: admin?products');
            } else {
                header('Location: shop');
            }
        }

        require_once(ROOT . '/view/product/edit.php');
        return true;
    }

    /**
     * actionDelete
     * Удаление товара
     * @param $idProduct
     * @return bool
     */
    public function actionDelete($idProduct)
    {
        $userId = User::checkLogged();
        $user   = User::getUserById($userId);
        Shop::checkShop(); //есть у пользователя магазин?
        if (isset($_SESSION['shop'])) {
            $shop = $_SESSION['shop'];
        } else {
            $shop = 'false';
        }
        if ($shop != 'false') {
            $isValid = Product ::checkProduct($idProduct); //Товар принадлежит пользователю?
            if ($isValid != false) {
                Product ::deleteProduct($idProduct);
            }
            if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != ""){
                $url = $_SERVER['HTTP_REFERER'];
            }else{
                $url = "shop";
            }
            header("Location: ".$url);
            die;
        } else {
            header("Location: index");
        }
        return true;
    }

}