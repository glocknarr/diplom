<?php

class CabinetController
{
    /**
     * Главная страница личного кабинета
     * @return bool
     */
    public function actionIndex()
    {
        $userId     = User ::checkLogged();
        $user       = User ::getUserById($userId);
        $address    = User ::checkUserAddress($userId);

        $allowed_ext = ['jpg', 'jpeg', 'png']; // Допустимые расширения аватарок
        $path = USERFILES . '/' . $userId . '/';
        foreach ($allowed_ext as $ext) {
            if (file_exists($path . 'avatar.' . $ext)) {
                $avatar = 'userfiles/' . $userId . '/' . 'avatar.' . $ext;
                break;
            } else {
                $avatar = DEFAULT_AVATAR;
            }
        }
        require_once(ROOT . '/view/cabinet/index.php');
        return true;
    }

    /**
     * Редактирование пользователя
     * @param string $id - если параметр не передан, то редактировать текущего
     * @return bool
     */
    public function actionEdit($id = "default")
    {
        if ($id == 'default') {
            $userId = User ::checkLogged();
        } else {
            $userId = (int)$id;
        }

        $user       = User ::getUserById($userId);
        $address    = User ::checkUserAddress($userId);

        $name           = $user['name'];
        $patronymic     = $user['patronymic'];
        $surname        = $user['surname'];
        $password       = $user['pass'];
        $email          = $user['email'];
        $sex            = $user['sex'];
        $birthday       = $user['birthday'];
        $phone          = $user['phone'];
        $web            = $user['web'];

        $result = false;

        if (isset($_POST['submit'])) {
            $name           = htmlspecialchars($_POST['name']);
            $patronymic     = htmlspecialchars($_POST['patronymic']);
            $surname        = htmlspecialchars($_POST['surname']);
            $password       = htmlspecialchars($_POST['password']);
            $email          = $_POST['email'];
            $birthday       = $_POST['birthday'];
            $phone          = htmlspecialchars($_POST['phone']);
            $web            = htmlspecialchars($_POST['web']);

            $errors         = false;

            if (!User ::checkName($name)) {
                $errors[] = "Имя не должно быть короче 2 символов.";
            }
            if (!User ::checkPassword($password)) {
                $errors[] = "Пароль не может быть короче 6 символов.";
            }
            if (!User ::checkEmail($email)) {
                $errors[] = "Неправильный email";
            }

            $editUser = array (
                'id'            => $userId,
                'name'          => $name,
                'patronymic'    => $patronymic,
                'surname'       => $surname,
                'password'      => $password,
                'email'         => $email,
                'birthday'      => $birthday,
                'phone'         => $phone,
                'web'           => $web
            );

            if ($errors == false) {
                $result = User ::edit($editUser);
                if ($id == 'default') {
                    header("Location: cabinet");
                }
            }
        }
        require_once(ROOT . '/view/cabinet/edit.php');
        return true;
    }

    public static function actionChangeAvatar()
    {
        $userId = User ::checkLogged();
        $handle = new Upload($_FILES['avatar']);
        if ($handle -> uploaded) {
            $handle -> allowed = array ('image/*'); // Только картинки
            $handle -> file_new_name_body = 'avatar'; // Имя нового файла
            $handle -> file_dst_name_ext = $handle -> file_src_name_ext; // Расширение не меняем
            $handle -> file_overwrite = true; // Перезаписывать, если существует
            $handle -> dir_auto_create = true; // Создавать папку получатель, если ее нет
            $handle -> dir_auto_chmod = true;
            $handle -> dir_chmod = 0777;
            $handle -> file_dst_path = USERFILES . "/" . $userId . "/"; // Папка получатель
            $allowed_extension = ['jpg', 'jpeg', 'png', 'gif'];
            foreach ($allowed_extension as $ext) { // сначала удаляем все предыдущие аватарки
                $fileName = USERFILES . '/' . $userId . '/avatar.' . $ext;
                if (file_exists($fileName)) {
                    unlink($fileName);
                }
            }
            $handle -> process($handle -> file_dst_path); // директория для загрузки
            if ($handle -> processed) {
                $handle -> clean();
                header("Location: cabinet");
            } else {
                echo 'error : ' . $handle -> error;
            }
            die;
        }
    }

    public static function actionAddress($action = 'index', $id = 'false')
    {
        if ($id != 'false') {
            $id = (int)$id;
        }
        $userId = $_SESSION['user'];
        $addresses = User ::checkUserAddress($userId);

        switch ($action) {
            case $action == 'index':
                require_once(ROOT . '/view/address/index.php');
                break;
            case $action == 'edit':
                $header         = "Изменение адреса";
                $addressData    = User ::getAddressById($id);
                if (isset($_POST['submit'])) {
                    $data               = $_POST;
                    $data['user_id']    = $userId;
                    $data['address_id'] = $id;
                    $newAddressId       = User::editAddress($data);
                    $addresses          = User ::checkUserAddress($userId);
                    $_POST = [];
                    require_once(ROOT . '/view/address/index.php');
                } else {
                    require_once(ROOT . '/view/address/edit.php');
                }
                break;
            case $action == 'delete':
                $delAddress = User::deleteAddress($id);
                $addresses  = User::checkUserAddress($userId);
                require_once(ROOT . '/view/address/index.php');
                break;
            case $action == 'add':
                //добавление адреса
                $header = "Добавление адреса";
                if (isset($_POST['submit'])) {
                    $data           = $_POST;
                    $data['user_id']= $userId;
                    $newAddressId   = User::addAddress($data);
                    $addresses      = User ::checkUserAddress($userId);
                    $_POST          = [];
                    require_once(ROOT . '/view/address/index.php');
                } else {
                    require_once(ROOT . '/view/address/edit.php');
                }
                break;
            default:
                header("Location: index");
                break;
        }
    }

}