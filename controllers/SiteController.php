<?php
class SiteController
{
    public function actionIndex($page = 1)
    {
        if((int)$page <= 0){
            $page = 1;
        }
        $categories     = ProductCategory::getProductCategoryList();
        $latestProduct  = Product::getProductsList($page);
        $total          = Product::countProducts();

        $pagination     = new Pagination($total, PRODUCTS_ON_PAGE, $page, 'page-');
        require_once ROOT.'/view/site/index.php';
        return true;

    }
}