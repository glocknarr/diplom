<?php

class ShopController
{
    private $tmp_file_name; // Имя временного файла картинки товара

    public function actionIndex()
    {
        $userId     = User::checkLogged();
        $user       = User::getUserById($userId);
        $shop       = Shop::checkShop(); //Проверяем, есть ли у пользователя магазин
        $dataShop   = Shop::getDataShop();
        if ($shop == false) {
            require_once(ROOT . '/view/shop/index.php');
            return true;
        }
        $myProducts = Shop::getMyProduct();
        require_once(ROOT . '/view/shop/index.php');
        return true;
    }

    /**
     * actionAddProduct
     * Добавление товаров в магазин
     * @return bool
     */
    public function actionAddProduct()
    {
        $userId             = User::checkLogged();
        $user               = User::getUserById($userId);
        $productCategory    = ProductCategory::getProductCategoryList();

        if (isset($_SESSION['tmp_picture']) && $_SESSION['tmp_picture'] != ''){
            $this->tmp_file_name = $_SESSION['tmp_picture'];
        }
        $tmp_pict = $this->tmp_file_name;
        if (isset($_POST['submit'])) {
            $tmp_pict       = '';
            $name           = $_POST['name'];
            $price          = intval($_POST['price']);
            $category       = $_POST['category'];
            $payment_info   = $_POST['payment_info'];
            $delivery_info  = $_POST['delivery_info'];
            $description    = $_POST['description'];

            isset($_POST['is_new'])     ? $is_new   = 1                     : $is_new = 0;
            isset($_POST['for_sale'])   ? $for_sale = 1                     : $for_sale = 0;
            isset($_POST['visible'])    ? $visible  = 1                     : $visible = 0;
            if (isset($_SESSION['tmp_picture']) && $_SESSION['tmp_picture'] != ''){
                $this->tmp_file_name = $_SESSION['tmp_picture'];
            }
            $this->tmp_file_name != ''  ? $fileName = $this->tmp_file_name  : $fileName = '';
            $newProduct = array(
                'name'          => $name,
                'price'         => $price,
                'category'      => $category,
                'payment_info'  => $payment_info,
                'delivery_info' => $delivery_info,
                'description'   => $description,
                'is_new'        => $is_new,
                'for_sale'      => $for_sale,
                'visible'       => $visible,
                'picture'       => $fileName
            );
            Shop::addProduct($newProduct);
            if ($fileName != ''){
                // Если временный файл существует, удаляем его
                if (file_exists(USERFILES . '/tmp/' . $fileName)){
                    unlink(USERFILES . '/tmp/' . $fileName);
                }
                $fileName               = '';
                $this->tmp_file_name    = '';
                $_SESSION['tmp_picture']= '';
            }
            header('Location: shop');
            die;
        }
        require_once(ROOT . '/view/shop/add.php');
        return true;
    }

    /**
     * actionCreateShop
     * Создание нового магазина. Если у пользователя магазин уже есть - перенаправление на индекс
     * @return bool
     */
    public function actionCreateShop()
    {
        $userId     = User::checkLogged();
        $user       = User::getUserById($userId);
        $dataShop   = Shop::getDataShop();
        $name       = '';
        $description = '';
        if (isset($_POST['submit'])) {
            if (Shop::checkShop()) {
                $errors[] = 'У пользователя уже существует магазин';
            }
            $errors = false;

            if (isset($_POST['name'])) {
                $name = htmlspecialchars($_POST['name']);
            } else {
                $errors[] = 'Ошибка в наименовании магазина';
            }

            if (isset($_POST['description'])) {
                if (true) {
                    $description = htmlspecialchars($_POST['description']);
                } else {
                    $errors[] = 'Ошибка в описании магазина';
                }
            }

            if ($errors == false) {
                //Ошибок нет, вызываем функцию регистрации магазина в базе
                $newShop = array(
                    'name' => $name,
                    'description' => $description
                );
                $result = Shop::createShop($newShop);
                header("Location: shop");
            } else {
                echo '<pre>';
                    print_r($errors);
                echo '</pre>';
                die;
            }
        }
        require_once(ROOT . '/view/shop/createShop.php');
        return true;
    }

    /**
     * Изменение настроек магазина
     * @param string $shopId
     * @return bool
     */
    public function actionConfigure($shopId = 'default')
    {
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if ($shopId == 'default'){
            $dataShop = Shop::getDataShop();
        } else {
            $dataShop = Shop::getDataShop($shopId);
        }
        $user = User::getUserById($userId);

        $name = $dataShop['name'];
        $description = $dataShop['description'];

        $errors = false;

        if (isset($_POST['submit'])) {
            $name           = htmlspecialchars($_POST['name']);
            $description    = htmlspecialchars($_POST['description']);

            $edit = array(
                'name'          => $name,
                'description'   => $description,
                'id'            => $dataShop['id']
            );

            if ($errors == false) {
                $result = Shop::editShop($edit);
                if ($access['name'] == 'admin'){
                    header('Location: admin?shops');
                } else {
                    header('Location: shop');
                }

            }
        }

        require_once(ROOT . '/view/shop/edit.php');
        return true;
    }

    /**
     * actionViewShop
     * Просмотр содержимого чужого магазина
     * @param $idShop
     * @return bool
     */
    public function actionViewShop($idShop)
    {
        $shopProduct    = Shop::getShopProduct($idShop);
        $categories     = ProductCategory::getProductCategoryList();
        $shopName       = Shop::getShopNameById($idShop);
        require_once(ROOT . '/view/shop/view.php');
        return true;
    }

    /**
     * Загрузка картинки товара во временную папку
     */
    public static function actionProductPicture()
    {
        $userId = User ::checkLogged();
        $handle = new Upload($_FILES['product-picture']);
        if ($handle->uploaded){
            $handle->allowed = array('image/*'); // Только картинки
            $handle->file_new_name_body = $handle->file_src_name_body; // Имя нового файла оставляем
            $handle->file_dst_name_ext = $handle->file_src_name_ext; // Расширение не меняем
            $handle->file_overwrite = false; // НЕ перезаписывать, если существует
            $handle->dir_auto_create = true; // Создавать папку получатель, если ее нет
            $handle->dir_auto_chmod = true;
            $handle->dir_chmod = 0777;
            $handle->file_dst_path = USERFILES . "/tmp/"; // Сохраняем во временную папку
            $handle->process($handle->file_dst_path); // директория для загрузки

            if ($handle->processed)
            {
                $_SESSION['tmp_picture'] = $handle->file_dst_name;
                $handle->clean();
                header("Location: ".$_SERVER['HTTP_REFERER']);
            } else {
                echo 'error : ' . $handle->error;
            }
            die;
        }
        
    }
}


