<?php

class AdminController
{
    public function actionIndex()
    {
        /**
         * Если пользователь админ, то показываем админку. Иначе - доступ запрещен
         */
        $userId     = User::checkLogged();
        $access     = User::getUserAccess($userId);
        $siteInfo   = Admin::getSiteInfo();
        if ($access['name'] == 'admin' && $siteInfo != false){
            require_once(ROOT . '/view/admin/index.php');
        } else {
            require_once(ROOT . '/view/admin/access_denied.php');
        }
    }


    /**
     * Информация по аккаунтам
     */
    public static function actionAccounts()
    {
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if ($access['name'] == 'admin'){
            $allAccounts = Admin::getAccountsInfo();
            foreach ($allAccounts as $account){
                if ($account['sex'] == 1){
                    $gender = 'муж.';
                } elseif ($account['sex'] == 2){
                    $gender = 'жен.';
                } else {
                    $gender = 'не определено';
                }
                $accountsInfo[]=[
                    'id'            => $account['id'],
                    'fio'           => $account['fio'],
                    'email'         => $account['email'],
                    'registration'  => $account['registration'],
                    'gender'        => $gender,
                    'phone'         => $account['phone'],
                    'web'           => $account['web']
                ];
            }
            require_once(ROOT . '/view/admin/accounts.php');
        } else {
            require_once(ROOT . '/view/admin/access_denied.php');
        }
    }

    /**
     * Информация по магазинам
     */
    public static function actionShops()
    {
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if ($access['name'] == 'admin'){
        $shopsInfo = Admin::getShopsInfo();
        require_once(ROOT . '/view/admin/shops.php');
        } else {
            require_once(ROOT . '/view/admin/access_denied.php');
        }
    }

    /**
     * Информация по товарам
     */
    public static function actionProducts()
    {
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if ($access['name'] == 'admin'){
        $allProducts = Admin::getProductsInfo();
        foreach ($allProducts as $product){
            $forSale = $product['for_sale'] == 1 ? 'Продается' : 'Нет';
            $productInfo[] = [
                'id'        => $product['id'],
                'name'      => $product['name'],
                'price'     => $product['price'],
                'shop'      => $product['shop'],
                'for_sale'  => $forSale
            ];
        }
        require_once(ROOT . '/view/admin/products.php');
        } else {
            require_once(ROOT . '/view/admin/access_denied.php');
        }
    }

}