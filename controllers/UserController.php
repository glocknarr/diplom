<?php

class UserController
{
    /**
     * actionRegister
     * Регистрация пользователя
     * @return bool
     */
    public function actionRegister()
    {
        $name       = '';
        $login      = '';
        $password   = '';
        $password2  = '';
        $email      = '';

        $result     = false;

        if (isset($_POST['submit'])) {
            $errors = false;
            if (!isset($_POST['check'])) {
                $errors[] = "Необходимо согласие на обработку и хранение персональных данных";
            }
            if (isset($_POST['name'])) {
                $name = htmlspecialchars($_POST['name']);
                if (!User ::checkName($name)) {
                    $errors[] = "Имя не должно быть короче двух символов";
                }
            } else {
                $errors[] = "Не задано имя пользователя!";
            }

            if (isset($_POST['login'])) {
                $login = htmlspecialchars($_POST['login']);
                if (!User ::checkLogin($login)) {
                    $errors[] = "Логин не должен быть короче двух символов";
                }
                if (!User ::checkLoginInBase($login)) {
                    $errors[] = "Такой логин уже существует";
                }

            } else {
                $errors[] = "Не задан логин!";
            }

            if (isset($_POST['password'])) {
                $pass = htmlspecialchars($_POST['password']);
                if (!User ::checkPassword($pass)) {
                    $errors[] = "Пароль не должен быть короче шести символов";
                }
                if ($_POST['password'] != $_POST['password2']) {
                    $errors[] = "Пароли не совпадают";
                }
            } else {
                $errors[] = "Не задан пароль!";
            }

            if (isset($_POST['email'])) {
                $email = $_POST['email'];
                if (!User ::checkEmail($email)) {
                    $errors[] = "Неправильный email";
                }
            } else {
                $errors[] = "Не задан email!";
            }

            if (isset($_POST['user_captcha']) && $_POST['user_captcha'] != ''){
                $captcha_code = strtolower($_POST['obr']);
                $value = strtolower($_POST['user_captcha']);
                if ($value != $captcha_code){
                    $errors[] = "Код с картинки введен не верно!";
                }

            } else {
                $errors[] = "Необходимо ввести код с картинки!";
            }

            if ($errors == false) {
                //Ошибок нет, регистрируем
                $newUser = array (
                    'name'      => $name,
                    'login'     => $login,
                    'password'  => $pass,
                    'email'     => $email,
                );
                $result = User ::register($newUser);
            }
        }
        require_once(ROOT . '/view/user/register.php');
        return true;
    }

    /**
     * actionLogin
     * Вход пользователя в учетную запись
     * @return bool
     */
    public function actionLogin()
    {
        $login      = '';
        $password   = '';
        if (isset($_POST['submit'])) {
            $login      = htmlspecialchars($_POST['login']);
            $password   = htmlspecialchars($_POST['password']);
            $errors     = false;

            if (!User ::checkLogin($login)) {
                $errors[] = 'Неправильный логин!';
            }

            if (!User ::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6 символов';
            }

            $userId = User ::checkUserData($login, $password);

            if ($userId == false) {
                $errors[] = 'Не правильные данные для входа на сайт!';
            } else {
                User ::auth($userId);
                header("Location: index");
            }
        }
        require_once(ROOT . '/view/user/login.php');
        return true;
    }

    /**
     * actionLogout
     * Разлогиниваем пользователя, удаляем его данные
     */
    public function actionLogout()
    {
        unset($_SESSION["user"]); //удаляем данные о пользователе
        unset($_SESSION["address"]); //удаляем данные об адресах доставки
        unset($_SESSION['shop']); //удаляем данные магазина

        header("Location: index");
    }

}