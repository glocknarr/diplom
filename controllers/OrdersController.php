<?php

class OrdersController
{
    /**
     * Список всех заказов в моем магазине
     * @return bool
     */
    public function actionIndex()
    {
        $orderList = Orders::getMyOrders();
        require_once(ROOT . '/view/order/index.php');
        return true;

    }

    /**
     * Просмотр заказа
     * @param $productid
     * @return bool
     */
    public function actionView($productid)
    {
        $productid = (int)$productid;
        $orderList = Orders::getMyOrders();
        foreach ($orderList as $order => $item){
            if ($order == $productid){
                $fullOrderInfo[] = $item;
            }
        }
        require_once(ROOT . '/view/order/view.php');
        return true;
    }

    public static function actionPurchases()
    {
        $userId     = User::checkLogged();
        $user       = User::getUserById($userId);
        $purchases  = Orders::getMyPurchases();
        require_once(ROOT . '/view/order/purchases.php');
        return true;
    }

}
