<?php
define('ROOT',  pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_DIRNAME));
define('COMPONENTS', ROOT . '/components');
define('USERFILES', ROOT . '/userfiles');
define('TMP', 'userfiles/tmp'); // Папка для временного хранения картинок товаров

define('DEFAULT_PICTURES', 'template/images/avatar/nopicture.jpg');
define('DEFAULT_AVATAR', 'template/images/avatar/nophoto.png');

define('PRODUCTS_ON_PAGE', 9); // Количество товаров на странице пагинации