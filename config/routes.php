<?php
/**
 * Маршруты для перенаправления по страницам
 */
return array(
	'product\?([0-9]+)'                 => 'product/view/$1',
	'product\?edit&([0-9]+)'            => 'product/edit/$1',
	'product\?delete&([0-9]+)'          => 'product/delete/$1',

	'catalog'                           => 'catalog/index',

    'cart\?([0-9]+)'                    => 'cart/index/$1',

    'admin\?accounts'                   => 'admin/accounts',
    'admin\?shops'                      => 'admin/shops',
    'admin\?products'                   => 'admin/products',
    'admin'                             => 'admin/index',

    'contacts'                          => 'contacts/index',

	'orders\?([0-9]+)'                  => 'orders/view/$1',
    'orders'                            => 'orders/index',
	'purchases'                         => 'orders/purchases',

	'category\?([0-9]+)&page-([0-9]+)'  => 'catalog/category/$1/$2',
	'category\?([0-9]+)'                => 'catalog/category/$1',

	'user\?register'                    => 'user/register',
	'user\?login'                       => 'user/login',
	'user\?logout'                      => 'user/logout',

	'avatar'                            => 'cabinet/changeAvatar',
	'cabinet\?edit&([0-9]+)'            => 'cabinet/edit/$1',
	'cabinet\?edit'                     => 'cabinet/edit',
    'cabinet\?address&add'              => 'cabinet/address/add',
    'cabinet\?address&(edit|delete)&([0-9]+)'    => 'cabinet/address/$1/$2',
    'cabinet\?address'                  => 'cabinet/address',
	'cabinet'                           => 'cabinet/index',

	'shop\?create'                      => 'shop/createShop',
	'shop\?add'                         => 'shop/addProduct',
	'shop\?configure&([0-9]+)'          => 'shop/configure/$1',
	'shop\?configure'                   => 'shop/configure',
	'shop\?([0-9]+)'                    => 'shop/viewShop/$1',
	'shop'                              => 'shop/index',
	'productPicture'                    => 'shop/productPicture',

	'about'                             => 'about/index',

	'index&page-([0-9]+)'               => 'site/index/$1',
	'index'                             => 'site/index',
);