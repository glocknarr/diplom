<?php

class Admin
{
    /**
     * Возвращает информацию о сайте для админки
     * @return array|bool
     */
    public static function getSiteInfo()
    {
        global $db;
        $request = [];
        // Проверяем еще раз, что бы пользователь был админом
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if($access['name'] != 'admin'){
            return false;
        }
        $query = "SELECT COUNT(*) FROM `users`;";
        $countUsers = $db->getValue($query);
        $request['countUsers'] = $countUsers;

        $query = "SELECT COUNT(*) FROM `shop`;";
        $countShops = $db->getValue($query);
        $request['countShops'] = $countShops;

        $query = "SELECT COUNT(*) FROM `products`;";
        $countProducts = $db->getValue($query);
        $request['countProducts'] = $countProducts;

        return $request;
    }

    /**
     * Получение информации по всем аккаунтам
     * @return array|bool|null
     */
    public static function getAccountsInfo()
    {
        global $db;
        // Проверяем еще раз, что бы пользователь был админом
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if($access['name'] != 'admin'){
            return false;
        }
        $query = "SELECT 
       `id`, 
       CONCAT(`surname`, ' ', `name`, ' ', `patronymic`) AS fio, 
       `email`, 
       `registration`, 
       `sex`,
       `phone`, 
       `web`   
        FROM `users`
        ORDER BY fio ASC;";
        $allUsers = $db -> getArray($query);
        return $allUsers;
    }

    /**
     * Информация по магазинам
     * @return array|bool|null
     */
    public static function getShopsInfo()
    {
        global $db;
        // Проверяем еще раз, что бы пользователь был админом
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if($access['name'] != 'admin'){
            return false;
        }
        $query = 'SELECT 
       shop.id, 
       shop.name, 
       shop.registration, 
       shop.description, 
       CONCAT(users.surname, " ", users.name, " ", users.patronymic) AS fio
        FROM `shop`, `users`
        WHERE shop.user_id = users.id';
        $allShops = $db->getArray($query);
        return $allShops;
    }

    public static function getProductsInfo()
    {
        global $db;
        // Проверяем еще раз, что бы пользователь был админом
        $userId = User::checkLogged();
        $access = User::getUserAccess($userId);
        if($access['name'] != 'admin'){
            return false;
        }
        $query = "SELECT 
       products.id,
       products.name,
       products.price,
       shop.name as shop,
       products.for_sale
        FROM `products`, `shop`
        WHERE products.shop_id = shop.id
        ORDER BY shop;";
        $allProducts = $db->getArray($query);
        return $allProducts;
    }
}