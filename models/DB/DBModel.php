<?php
final class DBModel {
    private $config; //Получение конфигов подключения
    private static $_instance;
    public $_connect;

    function __construct()
    {
        $configPath = ROOT . '/config/db_params.php';
        $this->config = include($configPath);
        $this->_connect = mysqli_connect(
            $this->config['HOST'],
            $this->config['USER'],
            $this->config['PASSWORD'],
            $this->config['DBNAME']);
        mysqli_set_charset($this->_connect, 'utf8');
    }

    private function connect()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * getValue
     * Получить одно значение
     * @param $query
     * @return bool|null
     */
    function getValue($query)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        if(isset($obj)){
            $info = mysqli_query($obj, $query);
            if(mysqli_num_rows($info)) {
                $data = mysqli_fetch_array($info);
                return $data[0];
            } else {
                return null;
            }
        } else {
            return false;
        }
    }

    /**
     * getRow
     * Получить строку таблицы
     * @param $query
     * @return array|bool|null
     */
    function getRow($query)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        if(isset($obj)){
            $wtf = mysqli_query($obj, $query);
            if(mysqli_num_rows($wtf)) {
                return mysqli_fetch_assoc($wtf);
            } else {
                return null;
            }
        } else {
            return false;
        }
    }

    /**
     * setValue
     * Добавить строку в таблицу
     * @param $query
     * @return bool|int|string
     */
    function setValue($query)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        if(isset($obj)){
            mysqli_query($obj, $query);
            return mysqli_insert_id($obj);
        } else {
            return false;
        }
    }

    /**
     * removeValue
     * Удалить строку из таблицы
     * @param $query
     * @return bool
     */
    function removeValue($query)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        if(isset($obj)){
            mysqli_query($obj, $query);
            return true;
        } else {
            return false;
        }
    }

    /**
     * getArray
     * Получить ассоц.массив записей
     * @param $query
     * @return array|bool|null
     */
    public function getArray($query)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        if(isset($obj)){
            $query = mysqli_query($obj, $query);
            $dataArray = mysqli_fetch_all($query, MYSQLI_ASSOC);
            return $dataArray;
        } else {
            return false;
        }
    }

    /**
     * updateValue
     * Изменить записи строки таблицы
     * @param $query
     * @return bool
     */
    function updateValue($query)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        if(isset($obj)){
            mysqli_query($obj, $query);
            return true;
        } else {
            return false;
        }
    }

    /**
     * escape
     * Проверка на допустимые символы
     * @param $str
     * @return bool|string
     */
    function escape($str)
    {
        $obj = $this->connect();
        $obj = $obj->_connect;
        return $obj ? mysqli_real_escape_string($obj, $str) : false;
    }

    /**
     * @static  getHash
     * Генерация хеш-кода
     * @param $size
     * @return string
     */
    static function getHash($size)
    {
        $str = "abcdefghijklmnopqrstuvwxyz0123456789";
        $hash = "";
        for ($i = 0; $i < $size; $i++) {
            $hash.= $str[rand(0, 35)];
        }
        return $hash;
    }

    //Отключаем возможность наследования класса
    private function __clone(){}
    private function __sleep(){}
    private function __wakeup(){}
}
