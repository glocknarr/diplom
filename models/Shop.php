<?php


class Shop
{
    /**
     * @static  checkShop
     * Проверяем есть ли у текущего пользователя свой магазин
     * @return bool
     */
    public static function checkShop()
    {
        if (isset($_SESSION['user'])) {
            global $db;
            $user   = $_SESSION['user'];
            $query  = "SELECT id FROM shop WHERE user_id='$user';";
            $result = $db->getRow($query);
            if ($result != '') {
                // если магазин есть, в сессию оставляем id магазина
                $_SESSION['shop'] = $result['id'];
                return true;
            }
            return false;
        } else {
            return false;
        }

    }


    /**
     * @static  getMyProduct
     * Возвращает список товаров данного магазина, текущего пользователя
     * @return array|bool|null
     */
    public static function getMyProduct()
    {
        global $db;
        $user   = $_SESSION['user'];
        $result = [];
        $query  = "SELECT products.id AS id, 
                    products.name AS name,
                    products.price AS price,
                    products.payment_info as payment_info,
                    products.delivery_info AS delivery_info,
                    products.is_new AS is_new,   
                    products.visible AS visible             
                    FROM products
                    LEFT JOIN shop s on products.shop_id = s.id
                    LEFT JOIN users u on s.user_id = u.id
                    WHERE u.id='$user' AND deleted = '0'; ";
        $request = $db->getArray($query);
        foreach ($request as $product => $item){
            $productId  = $item['id'];
            $photo      = Product::getProductPicture($productId);
            if ($photo){
                $filePath = 'userfiles/' . $user . '/shop/' . $productId . '/' . $photo[0]['file_name'];
                if (file_exists($filePath)) {
                    $item['picture'] = $filePath;
                } else {
                    $item['picture'] = DEFAULT_PICTURES;
                }
            } else {
                $item['picture'] = DEFAULT_PICTURES;
            }
            $result[] = $item;
        }
        return $result;
    }

    /**
     * @static  getShopProduct
     * Просмотр товаров чужого магазина
     * @param $idShop
     * @return array|bool|null
     */
    public static function getShopProduct($idShop)
    {
        global $db;
        $result = [];
        $query  = "SELECT products.id AS id, 
                    products.name AS name,
                    products.price AS price,
                    products.payment_info as payment_info,
                    products.delivery_info AS delivery_info,
                    products.is_new AS is_new,   
                    products.for_sale AS for_sale,   
                    products.visible AS visible             
                    FROM products
                    LEFT JOIN shop s on products.shop_id = s.id
                    WHERE s.id='$idShop' AND products.deleted = '0';";
        $request = $db->getArray($query);
        foreach ($request as $productInfo => $index) {
            $query  = "SELECT `user_id` FROM `shop` WHERE `id` = '{$idShop}';";
            $seller = $db -> getValue($query);
            $pictureFile = Product::getProductPicture($index['id']);
            if ($pictureFile) {
                $filePath = 'userfiles/' . $seller . '/shop/' . $index['id'] . '/' . $pictureFile[0]['file_name'];
                if (file_exists($filePath)) {
                    $index['picture'] = $filePath;
                } else {
                    $index['picture'] = DEFAULT_PICTURES;
                }
            } else {
                $index['picture'] = DEFAULT_PICTURES;
            }
            $result[] = $index;
        }
        return $result;
    }

    /**
     * @static  getDataShop
     * Возвращает массив с названием и описанием магазина для редактирования
     * @return array|bool|null
     */
    public static function getDataShop($shopId = 'default')
    {
        global $db;
        $user   = $_SESSION['user'];
        $access = User::getUserAccess($user);
        if ($access['name'] == 'admin'){
            $query = "SELECT * FROM `shop` WHERE `id` = '{$shopId}';";
        } else {
            $query = "SELECT * FROM shop WHERE user_id = '$user'";
        }
        $result = $db->getRow($query);
        return $result;

    }

    /**
     * @static  getShopName
     * Возвращает название магазина, к которому принадлежит товар
     * @param $idProduct
     * @return array|bool|null
     */
    public static function getShopName($idProduct)
    {
        global $db;
        $query  = "SELECT shop.name AS name, shop.id AS id FROM shop
                LEFT JOIN products p on shop.id = p.shop_id
                WHERE p.id = $idProduct ;";
        $result = $db ->getRow($query);
        return $result;
    }

    /**
     * @static  getShopNameById
     * Возвращает данные магазина по id магазина
     * @param $idShop
     * @return array|bool|null
     */
    public static function getShopNameById($idShop)
    {
        global $db;
        $query  = "SELECT * FROM shop WHERE id='$idShop'";
        $result = $db ->getRow($query);
        return $result;
    }

    /**
     * @static  createShop
     * Создание магазина, если у пользователя его нет
     * @param array $newShop
     * @return bool
     */
    public static function createShop(array $newShop)
    {
        if (!self::checkShop()) {
            global $db;
            $name           = $newShop['name'];
            $description    = $newShop['description'];
            $user           = $_SESSION['user'];

            $query  = "INSERT INTO shop (name,user_id,description) VALUES ('$name', '$user','$description');";
            $result = $db -> setValue($query);

        } else {
            return false; //У пользователя уже есть магазин, новый не создаем
        }
        return true;
    }

    /**
     * @static  editShop
     * Редактирование данных магазина
     * @param array $newData
     * @return bool
     */
    public static function editShop(array $newData)
    {
        global $db;
        $name           = $newData['name'];
        $description    = $newData['description'];
        $user           = $_SESSION['user'];
        $access         = User::getUserAccess($user);
        if ($access['name'] == 'admin'){
            $query = "UPDATE shop SET name ='$name', description = '$description' WHERE `id` = '{$newData['id']}';";
        } else {
            $query = "UPDATE shop SET name ='$name', description = '$description' WHERE `user_id` = '$user';";
        }

        $result = $db->updateValue($query);
        return $result;
    }

    /**
     * @static  addProduct
     * Добавление товара в магазин пользователя
     * @param array $newProduct
     * @return bool
     */
    public static function addProduct(array $newProduct)
    {
        global $db;
        $user           = $_SESSION['user'];
        $query          = "SELECT id FROM shop WHERE user_id='$user';";
        $request        = $db->getRow($query);
        $shopId         = $request['id'];
        $name           = $newProduct['name'];
        $price          = intval($newProduct['price']);
        $category       = intval($newProduct['category']);
        $payment_info   = $newProduct['payment_info'];
        $delivery_info  = $newProduct['delivery_info'];
        $description    = $newProduct['description'];
        $is_new         = $newProduct['is_new'];
        $for_sale       = $newProduct['for_sale'];
        $visible        = $newProduct['visible'];
        $picture        = $newProduct['picture'];

        $query = "INSERT INTO products (
                      shop_id,
                      name,
                      price,
                      category,
                      payment_info,
                      delivery_info,
                      description,
                      is_new,
                      visible,
                      for_sale) 
                      VALUES (
                              '$shopId',
                              '$name', 
                              '$price', 
                              '$category', 
                              '$payment_info', 
                              '$delivery_info', 
                              '$description', 
                              '$is_new', 
                              '$visible', 
                              '$for_sale');";
        $newProductId = $db->setValue($query);

        if ($picture != '' && $newProductId){
            // копируем картинку в папку магазина по шаблону:
            // userfiles/id-пользователя/shop/id-товара/picture.jpg

            $dst_dir = USERFILES . '/' . $user . '/shop/' . $newProductId; // Папка-получатель
            if (!file_exists($dst_dir)){
                mkdir($dst_dir);
            }
            copy(USERFILES . '/tmp/' . $picture, $dst_dir . '/' . $picture);
            unlink(USERFILES . '/tmp/' . $picture);
            $_SESSION['tmp_picture'] = '';

            $query = "INSERT INTO `photo` (product_id, file_name) 
                      VALUES ('{$newProductId}', '{$picture}');";
            $result = $db->setValue($query);
        }
        $_SESSION['tmp_picture'] = '';
        return true;
    }
}