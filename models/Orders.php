<?php

class Orders
{
    /**
     * Функция получения списка заказов в моем магазине
     * Получаем все заказы. Новые потом отбираем отдельно
     * @return mixed
     */
    public static function getMyOrders()
    {
        global $db;
        $userId = $_SESSION['user']; //Владелец магазина
        $query = "SELECT `orders`.* FROM `orders` 
                  LEFT JOIN `shop` shop on `orders`.`shop_id` = shop.id
                  LEFT JOIN `users` seller on `shop`.`user_id` = seller.id
                  WHERE `shop`.`user_id` = '{$userId}'
                  ORDER BY `order_date` DESC;";
        $orders = $db -> getArray($query);
        
        $orderList = [];
        foreach ($orders as $order => $index) {
            // Получаем инфо о товаре
            $query = "SELECT `name`, `price`, `payment_info`, `delivery_info`, `description`, `visible`, `for_sale` 
            FROM `products` 
            WHERE id = '{$index['product_id']}';";
            $product_info = $db -> getArray($query);
            foreach ($product_info as $product) {
                $orderList[$index['id']]['product_info'] = $product;
            }
            // Получаем инфо о покупателе
            $address = $index['address'];
            $orderList[$index['id']]['customer_info'] =
                [
                    'id'            => $index['user_id'],
                    'name'          => $index['customer_name'],
                    'surname'       => $index['customer_surname'],
                    'patronymic'    => $index['customer_patronymic'],
                    'phone'         => $index['customer_phone'],
                    'email'         => $index['customer_email'],
                    'address'       => $address
                ];
            $pictureFile = Product::getProductPicture($index['product_id']);
            if ($pictureFile) {
                $filePath = 'userfiles/' . $userId . '/shop/' . $index['product_id'] . '/' . $pictureFile[0]['file_name'];
                if (file_exists($filePath)) {
                    $index['picture'] = $filePath;
                } else {
                    $index['picture'] = DEFAULT_PICTURES;
                }
            } else {
                $index['picture'] = DEFAULT_PICTURES;
            }


            // Добавляем остальные поля
            $orderList[$index['id']]['price']           = $index['total'];
            $orderList[$index['id']]['order_date']      = $index['order_date'];
            $orderList[$index['id']]['delivery_date']   = $index['delivery_date'];
            $orderList[$index['id']]['description']     = $index['description'];
            $query = "SELECT `status` FROM `order_status` WHERE `id` = '{$index['status']}';";
            $orderList[$index['id']]['status']          = $db -> getValue($query);
            $orderList[$index['id']]['picture']         = $index['picture'];
        }
        return $orderList;
    }

    /**
     * Оформление покупки
     * @param array $info - информация о товаре, покупателе и т.д.
     * @return bool - если true, значит покупка оформлена
     */
    public static function buyProduct(array $info)
    {
        global $db;

        if (empty($info)) {
            return false;
        }

        // Проверка обязательных параметров
        if ($info['product_Id'] == '') {
            return false;
        }
        if ($info['customer_Id'] == '') {
            return false;
        }
        if ($info['order_name'] == '') {
            return false;
        }
        if ($info['order_email'] == '') {
            return false;
        }
        if ($info['address'] == '') {
            return false;
        }

        $query = "SELECT `shop_id`, `price` FROM `products` WHERE `id` = '{$info['product_Id']}';";
        $product_info = $db -> getArray($query);
        $shop_id = $product_info[0]['shop_id'];
        $total = $product_info[0]['price'];
        $query = "SELECT `id` FROM `order_status` WHERE `status` = 'Новый';";
        $status = $db -> getValue($query);

        // Запись в базу
        $query = "INSERT INTO `orders` 
  (`shop_id`, 
   `product_id`, 
   `user_id`, 
   `customer_name`, 
   `customer_surname`, 
   `customer_patronymic`, 
   `customer_phone`, 
   `total`, 
   `status`, 
   `address`, 
   `customer_email`, 
   `description`) 
VALUES 
       ('$shop_id', 
        '{$info['product_Id']}', 
        '{$info['customer_Id']}', 
        '{$info['order_name']}', 
        '{$info['order_surname']}', 
        '{$info['order_patronymic']}', 
        '{$info['order_phone']}', 
        '$total', 
        '$status', 
        '{$info['address']}', 
        '{$info['order_email']}', 
        '{$info['order_description']}');";

        $order_id = $db -> setValue($query);

        return true;
    }

    public static function getMyPurchases()
    {
        global $db;
        $userId = User ::checkLogged();
        $result = [];
        $query = "SELECT 
                   orders.id          AS order_id, 
                   orders.shop_id     AS shop_id, 
                   orders.product_id  AS product_id, 
                   orders.total       AS price, 
                   orders.order_date  AS order_date, 
                   os.status          AS order_status,  
                   p.name             AS product_name, 
                   p.description      AS product_description, 
                   s.name             AS shop_name  
                  FROM `orders`
                  LEFT JOIN products p on orders.product_id = p.id
                  LEFT JOIN shop s on orders.shop_id = s.id
                  LEFT JOIN order_status os on orders.status = os.id
                  WHERE `orders`.`user_id` = '{$userId}' 
                  ORDER BY `order_date` DESC ;";
        $request = $db -> getArray($query);
        foreach ($request as $productInfo => $index) {
            $query = "SELECT `user_id` FROM `shop` WHERE `id` = '{$index['shop_id']}';";
            $seller = $db -> getValue($query);
            $pictureFile = Product::getProductPicture($index['product_id']);
            if ($pictureFile) {
                $filePath = 'userfiles/' . $seller . '/shop/' . $index['product_id'] . '/' . $pictureFile[0]['file_name'];
                if (file_exists($filePath)) {
                    $index['picture'] = $filePath;
                } else {
                    $index['picture'] = DEFAULT_PICTURES;
                }
            } else {
                $index['picture'] = DEFAULT_PICTURES;
            }
            $result[] = $index;
        }
        return $result;
    }
}