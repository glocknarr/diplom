<?php


class Product
{
    /**
     * Количество товаров, выводимое на странице.
     */
    const SHOW_BY_DEFAULT = PRODUCTS_ON_PAGE;

    /**
     * @static  getLatestProduct
     * Получение списка всех товаров на сайте.
     * @return array|bool|null
     */
    public static function getLatestProduct()
    {
        global $db;
        $productList = [];
        $query = "SELECT * FROM `products`
                WHERE visible = '1' AND deleted = '0'
                ORDER BY id DESC
                ";
        $productList = $db -> getArray($query);
        if (!empty($productList)) {
            return $productList;
        } else {
            echo "Ошибка получения списка последних товаров!<br>";
            return false;
        }
    }

    /**
     * @static  getProductsListByCategory
     * Получение списка товаров конкретной выбранной категории
     * @param bool $categoryId
     * @param int  $page
     * @return array|bool|null
     */
    public static function getProductsListByCategory($categoryId = false, $page = 1)
    {
        $result = [];
        if ($categoryId) {
            $page   = intval($page);
            $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
            global $db;
            $query  = "SELECT * FROM products
              WHERE visible='1' AND category = $categoryId AND deleted = '0'
              ORDER BY id DESC 
              LIMIT " . self::SHOW_BY_DEFAULT
                . " OFFSET " . $offset;
            $request = $db -> getArray($query);
            foreach ($request as $productInfo => $index) {
                $query          = "SELECT `user_id` FROM `shop` WHERE `id` = '{$index['shop_id']}';";
                $seller         = $db -> getValue($query);
                $pictureFile    = self ::getProductPicture($index['id']);
                if ($pictureFile) {
                    $filePath   = 'userfiles/' . $seller . '/shop/' . $index['id'] . '/' . $pictureFile[0]['file_name'];
                    if (file_exists($filePath)) {
                        $index['picture'] = $filePath;
                    } else {
                        $index['picture'] = DEFAULT_PICTURES;
                    }
                } else {
                    $index['picture'] = DEFAULT_PICTURES;
                }
                $result[] = $index;
            }
            return $result;
        }
    }

    /**
     * Получение списка товаров главной страницы для пагинации
     * @param int $page
     * @return array|bool|null
     */
    public static function getProductsList($page = 1)
    {
        $page   = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        global $db;
        $query  = "SELECT * FROM products
              WHERE visible='1' AND deleted = '0'
              ORDER BY id DESC 
              LIMIT " . self::SHOW_BY_DEFAULT
            . " OFFSET " . $offset;
        $request = $db -> getArray($query);
        foreach ($request as $productInfo => $index) {
            $query          = "SELECT `user_id` FROM `shop` WHERE `id` = '{$index['shop_id']}';";
            $seller         = $db -> getValue($query);
            $pictureFile    = self ::getProductPicture($index['id']);
            if ($pictureFile) {
                $filePath   = 'userfiles/' . $seller . '/shop/' . $index['id'] . '/' . $pictureFile[0]['file_name'];
                if (file_exists($filePath)) {
                    $index['picture'] = $filePath;
                } else {
                    $index['picture'] = DEFAULT_PICTURES;
                }
            } else {
                $index['picture'] = DEFAULT_PICTURES;
            }
            $result[] = $index;
        }
        return $result;
    }

    /**
     * @static  getProductById
     * Получение товара по id
     * @param $id
     * @return array|bool|null
     */
    public static function getProductById($id)
    {
        $id = intval($id);

        if ($id) {
            global $db;
            $query      = "SELECT * FROM products WHERE id = $id";
            $request    = $db -> getRow($query);
            $query      = "SELECT `user_id` FROM `shop` WHERE `id` = '{$request['shop_id']}';";
            $seller     = $db -> getValue($query);
            $pictureFile = self ::getProductPicture($id);
            if ($pictureFile) {
                $filePath = 'userfiles/' . $seller . '/shop/' . $id . '/' . $pictureFile[0]['file_name'];
                if (file_exists($filePath)) {
                    $request['picture'] = $filePath;
                } else {
                    $request['picture'] = DEFAULT_PICTURES;
                }
            } else {
                $request['picture'] = DEFAULT_PICTURES;
            }
            return $request;
        }
    }


    /**
     * @static  editProduct
     * Редактирование товара
     * @param array $product
     * @return bool
     */
    public static function editProduct(array $product)
    {
        global $db;
        $user           = $_SESSION['user'];
        $shop_id        = $product['shop_id'];
        $idProduct      = $product['product'];
        $name           = $product['name'];
        $price          = $product['price'];
        $category       = $product['category'];
        $payment_info   = $product['payment_info'];
        $delivery_info  = $product['delivery_info'];
        $description    = $product['description'];
        $is_new         = $product['is_new'];
        $for_sale       = $product['for_sale'];
        $visible        = $product['visible'];
        $picture        = $product['picture'];

        $query = "UPDATE products SET 
                  name          ='$name', 
                  price         ='$price',
                  category      ='$category',
                  payment_info  ='$payment_info',
                  delivery_info ='$delivery_info',
                  description   ='$description',
                  is_new        ='$is_new',
                  visible       ='$visible',
                  for_sale      ='$for_sale'
                  WHERE shop_id ='$shop_id' AND id='$idProduct';";
        $result = $db -> updateValue($query);

        if ($picture != '') {
            // копируем картинку в папку магазина по шаблону:
            // userfiles/id-пользователя/shop/id-товара/picture.jpg

            $dst_dir = USERFILES . '/' . $user . '/shop/' . $idProduct; // Папка-получатель

            if (!file_exists($dst_dir)) {
                mkdir($dst_dir, 0777, true);
            }
            
            copy(USERFILES . '/tmp/' . $picture, $dst_dir . '/' . $picture);
            unlink(USERFILES . '/tmp/' . $picture);
            $_SESSION['tmp_picture'] = '';

            $query = "SELECT `id` FROM `photo` WHERE product_id = '{$idProduct}';";
            $productIsset = $db -> getValue($query);
            if ($productIsset) {
                $query = "UPDATE `photo` SET file_name = '{$picture}' 
                      WHERE product_id = '{$idProduct}';";
            } else {
                $query = "INSERT INTO `photo` (`product_id`, `file_name`) 
                      VALUES ('{$idProduct}', '{$picture}');";
            }
            $ret = $db -> setValue($query);

        }
        $_SESSION['tmp_picture'] = '';

        return true;
    }

    /**
     * @static  deleteProduct
     * Удаление товара
     * @param $idProduct
     * @return bool
     */
    public static function deleteProduct($idProduct)
    {
        global $db;
        $productFolder = USERFILES . '/' . $_SESSION['user'] . '/shop/' . $idProduct; // Папка хранения картинок товара
        $query = "SELECT COUNT(id) FROM `orders` WHERE `product_id` = '{$idProduct}';";
        $orderList = $db -> getValue($query);
        if (!$orderList) { // если заказов нет, удаляем товар
            // Сначала удаляем картинку товара!
            $photoId = self ::getProductPicture($idProduct);
            if ($photoId) {
                // удаляем из базы
                $sel = "DELETE FROM `photo` WHERE `product_id` = '{$idProduct}';";
                $db -> removeValue($sel);
                // удаляем файлы
                foreach ($photoId as $file) {
                    $path = $productFolder . '/' . $file['file_name'];
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
                // удаляем пустую папку
                if (file_exists($productFolder)) {
                    rmdir($productFolder);
                }
            }

            $query = "DELETE FROM products WHERE id='{$idProduct}';";
            $result = $db -> removeValue($query);
            return true;
        }
        $query = "UPDATE `products` SET
                  deleted = '1'
                  WHERE `id` = '{$idProduct}';";
        $result = $db -> setValue($query);
        return true;
    }

    /**
     * @static  checkProduct
     * Проверяем принадлежит ли товар текущему пользователю
     * @param $idProduct
     * @return bool
     */
    public static function checkProduct($idProduct)
    {
        global $db;
        $userId     = User ::checkLogged();
        $user       = User ::getUserById($userId);

        $query = "SELECT products.id AS id FROM products
                LEFT JOIN shop s on products.shop_id = s.id
                LEFT JOIN users u on s.user_id = u.id
                WHERE u.id = '$userId' AND products.id = '$idProduct';";
        $result = $db -> getRow($query);
        if ($result['id'] == '') {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Расчет количества товаров.
     * @param string $idCategory - если передан id категории, то считается в ней
     * @return int
     */
    public static function countProducts($idCategory = 'false')
    {
        global $db;
        if ($idCategory != 'false') {
            $idCategory = (int)$idCategory;
            $category = "AND `products`.`category` = '{$idCategory}'";
        } else {
            $category = '';
        }
        $query = "SELECT count(id) AS cnt FROM products
                  WHERE visible='1' AND deleted = '0' {$category};";
        $request = $db -> getRow($query);
        return $request['cnt'];
    }

    /**
     * Получение картинок для товара
     * @param $productId
     * @return array|bool|null
     */
    public static function getProductPicture($productId)
    {
        global $db;
        $query  = "SELECT * FROM `photo` WHERE `product_id` = '{$productId}';";
        $photos = $db -> getArray($query);
        return $photos;
    }

}