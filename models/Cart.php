<?php

// Так как магазин товаров ручной работы, то все покупки будут совершаться индивидуально
// по каждому товару.
// Нахватать полную корзину чего попало не получится.

class Cart
{
    public static function buyProduct($id)
    {
        global $db;

        $query          = "SELECT * FROM `products` WHERE `id` = '{$id}';";
        $request        = $db->getArray($query);
        $query          = "SELECT `user_id` FROM `shop` WHERE `id` = '{$request[0]['shop_id']}';";
        $seller         = $db -> getValue($query);
        $pictureFile    = Product::getProductPicture($id);
        if ($pictureFile) {
            $filePath   = 'userfiles/' . $seller . '/shop/' . $id . '/' . $pictureFile[0]['file_name'];
            if (file_exists($filePath)) {
                $request[0]['picture'] = $filePath;
            } else {
                $request[0]['picture'] = DEFAULT_PICTURES;
            }
        } else {
            $request[0]['picture'] = DEFAULT_PICTURES;
        }
        return $request;
    }

    /**
     * Получаем некоторую информацию о покупателе
     * Ограниченный вывод данных, что бы не сливать лишнюю информацию
     * @param $id - идентификатор текущего пользователя
     * @return bool|array - массив полученных данных. В случае неудачи возвращает false
     */
    public static function getCustomerInfo($id)
    {
        if ((int)$id){
            $fullUserInfo               = User::getUserById($id);
            $customerInfo['id']         = $fullUserInfo['id'];
            $customerInfo['name']       = $fullUserInfo['name'];
            $customerInfo['surname']    = $fullUserInfo['surname'];
            $customerInfo['patronymic'] = $fullUserInfo['patronymic'];
            $customerInfo['email']      = $fullUserInfo['email'];
            $customerInfo['phone']      = $fullUserInfo['phone'];
            $customerInfo['web']        = $fullUserInfo['web'];
            $customerInfo['address']    = User::checkUserAddress($id);
            return $customerInfo;
        }
        return false;
    }
}