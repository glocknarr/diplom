<?php

class User
{
    /**
     * @static  register
     * Регистрация пользователя
     * @param array $newUser
     * @return bool
     */
    public static function register(array $newUser)
    {
        global $db;

        $name       = $newUser['name'];
        $login      = $newUser['login'];
        $password   = password_hash($newUser['password'], PASSWORD_DEFAULT);
        $email      = $newUser['email'];

        $query      = "INSERT INTO users (name,login,pass,email) VALUES ('$name', '$login','$password','$email')";
        $request    = $db -> setValue($query);
        return true;

    }

    /**
     * @static  edit
     * Редактирование данных пользователя
     * @param array $newUser
     * @return bool
     */
    public static function edit(array $newUser)
    {
        global $db;

        $id             = $newUser['id'];
        $name           = $newUser['name'];
        $patronymic     = $newUser['patronymic'];
        $surname        = $newUser['surname'];
        $password       = password_hash($newUser['password'], PASSWORD_DEFAULT);
        $email          = $newUser['email'];
        $birthday       = $newUser['birthday'];
        $phone          = $newUser['phone'];
        $web            = $newUser['web'];

        $query = "UPDATE users 
        SET name    = '$name',
        surname     = '$surname',
        patronymic  = '$patronymic',
        pass        = '$password', 
        birthday    = '$birthday',
        email       = '$email',
        phone       = '$phone',
        web         = '$web'
        WHERE id    = '{$id}';";
        $result     = $db -> updateValue($query);
        return $result;

    }

    /**
     * @static  checkName
     * Проверка имени пользователя на корректность
     * @param $name
     * @return bool
     */
    public static function checkName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @static  checkPassword
     * Проверка пароля на корректность
     * @param $password
     * @return bool
     */
    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        if (preg_match("#^[aA-zZ0-9\-_]+$#",$password)) {
            return true;
        }
        return false;
    }

    /**
     * Проверка строки на допустимые символы
     * Допустимы буквы, цифры, тире, подчеркивание
     * @param $str
     * @return bool
     */

    /**
     * @static  checkEmail
     * Проверка емейла на корректность
     * @param $email
     * @return bool
     */
    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;

    }

    /**
     * @static  checkLogin
     * Проверка логина на корректность
     * @param $login
     * @return bool
     */
    public static function checkLogin($login)
    {
        if (strlen($login) < 2) {
            return false;
        }
        if (!preg_match("#^[aA-zZ0-9\-_]+$#",$login)) {
            return false;
        }
        return true;
    }

    /**
     * @static  checkLoginInBase
     * Проверка логина на существование в базе.
     * @param $login
     * @return bool
     */
    public static function checkLoginInBase($login)
    {
        global $db;
        $login      = htmlspecialchars($login);
        $query      = "SELECT * FROM users WHERE login='$login';";
        $request    = $db -> getRow($query);
        if ($request) {
            //такой логин уже есть. Ошибка
            return false;
        }
        return true;
    }

    /**
     * @static  checkUserData
     * Проверяем есть ли такой пользователь
     * @param $login
     * @param $password
     * @return bool
     */
    public static function checkUserData($login, $password)
    {
        global $db;
        $login      = htmlspecialchars(stripslashes($login));
        $password   = htmlspecialchars(stripslashes($password));

        $query      = "SELECT * FROM users WHERE login = '$login';";
        $user       = $db -> getRow($query);
        if (password_verify($password, $user['pass'])) {
            return $user['id'];
        }
        return false;

    }

    /**
     * @static  checkUserAddress
     * Получение списка адресов доставки пользователя
     * @param $userId
     * @return array
     */
    public static function checkUserAddress($userId)
    {
        global $db;
        $query      = "SELECT * FROM address WHERE user_id = '$userId'";
        $result     = $db -> getArray($query);
        $address    = [];
        foreach ($result as $res) {
            $address[$res['id']] = self ::getAddress($res);
        }
        return $address;
    }

    /**
     * Получаем данные адреса для редактирования
     * @param $addressId
     * @return array
     */
    public static function getAddressById($addressId)
    {
        global $db;
        $addressId  = (int)$addressId;
        $query      = "SELECT * FROM `address` WHERE id = '{$addressId}';";
        $request    = $db -> getArray($query);
        return $request[0];
    }

    /**
     * Редактирование адреса
     * @param array $dataAddress
     * @return bool
     */
    public static function editAddress(array $dataAddress)
    {
        global $db;
        $user_id        = $dataAddress['user_id'];
        $address_id     = $dataAddress['address_id'];
        $post_index     = $dataAddress['new-post-index'];
        $city           = $dataAddress['new-city'];
        $street         = $dataAddress['new-street'];
        $house          = $dataAddress['new-house'];
        $corps          = $dataAddress['new-corps'];
        $apartment      = $dataAddress['new-apartment'];
        $other_info     = $dataAddress['new-other-info'];

        $query = "UPDATE `address` SET 
                    user_id     = '$user_id', 
                    post_index  = '$post_index', 
                    city        = '$city', 
                    street      = '$street', 
                    house       = '$house', 
                    corps       = '$corps', 
                    apartment   = '$apartment', 
                    other_info  = '$other_info'
                  WHERE id      = '{$address_id}';";
        $addressId = $db->updateValue($query);
        return $addressId;
    }

    /**
     * Добавление нового адреса
     * @param array $dataAddress - данные нового адреса
     * @return string  - id добавленного адреса
     */
    public static function addAddress(array $dataAddress)
    {
        global $db;
        $user_id        = $dataAddress['user_id'];
        $post_index     = $dataAddress['new-post-index'];
        $city           = $dataAddress['new-city'];
        $street         = $dataAddress['new-street'];
        $house          = $dataAddress['new-house'];
        $corps          = $dataAddress['new-corps'];
        $apartment      = $dataAddress['new-apartment'];
        $other_info     = $dataAddress['new-other-info'];

        $query = "INSERT INTO `address`(user_id, post_index, city, street, house, corps, apartment, other_info) 
VALUES ('$user_id', '$post_index', '$city', '$street', '$house', '$corps', '$apartment', '$other_info')";
        $addressId = $db->setValue($query);
        return $addressId;        // Возвращаем Id нового адреса
    }

    /**
     * Удаление адреса пользователя
     * @param $addressId
     */
    public static function deleteAddress($addressId)
    {
        global $db;
        $addressId  = (int)$addressId;
        $query      = "DELETE FROM `address` WHERE id = '{$addressId}';";
        $db->removeValue($query);
    }

    /**
     * @static  getAddress
     * Формирует строку из заполненных данных адреса
     * @param array $address
     * @return string
     */
    public static function getAddress(array $address)
    {
        $result = "";
        if (isset($address['post_index']) && $address['post_index'] != 0) {
            $result .= $address['post_index'] . ', ';
        }
        if (isset($address['city']) && $address['city'] != '') {
            $result .= $address['city'] . ', ';
        }
        if (isset($address['street']) && $address['street'] != '') {
            $result .= $address['street'] . ', ';
        }
        if (isset($address['house']) && $address['house'] != 0) {
            $result .= $address['house'] . ', ';
        }
        if (isset($address['corps']) && $address['corps'] != '') {
            $result .= 'корп.' . $address['corps'] . ', ';
        }
        if (isset($address['apartment']) && $address['apartment'] != 0) {
            $result .= 'кв.' . $address['apartment'] . ', ';
        }
        return $result;
    }

    /**
     * @static  getBirthDay
     * Для вывода на форме. Выводит дату, если есть, или пустую строку, что бы не показывать дату 0000-00-00
     * @param $birthday
     * @return string
     */
    public static function getBirthDay($birthday)
    {
        if ($birthday != '0000-00-00') {
            return $birthday;
        }
        return '';
    }

    /**
     * @static  getSex
     * Возвращает пол строкой для вывода на форму
     * если пол равен:
     * 1 - возвращает "муж."
     * 2 - возвращает "жен."
     * иначе - пустую строку
     * @param $sex
     * @return string
     */
    public static function getSex($sex)
    {
        if ($sex == 1) {
            return 'муж.';
        } else {
            if ($sex == 2) {
                return 'жен.';
            } else {
                return '';
            }
        }
    }

    /**
     * @static  auth
     * Стартуем сессию пользователя
     * @param $userId
     */
    public static function auth($userId)
    {
        $userId                 = intval($userId);
        $_SESSION['user']       = $userId;
        $_SESSION['address']    = self ::checkUserAddress($userId);
    }

    /**
     * @static  checkLogged
     * Проверяем залогинился ли пользователь
     * @return mixed
     */
    public static function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        header("Location: user?login");
    }

    /**
     * @static  isGuest
     * Это гость?
     * @return bool
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;

    }

    /**
     * @static  getUserById
     * Получение данных пользователя по id
     * @param $userId
     * @return array|bool|null
     */
    public static function getUserById($userId)
    {
        global $db;
        $userId = intval($userId);
        $query  = "SELECT * FROM `users` WHERE `id` = '$userId';";
        $result = $db -> getRow($query);
        $result['pass'] = ''; // очищаем пароль, что бы не отображать хеш
        return $result;
    }

    /**
     * Получение прав пользователя
     * @param $userId
     * @return array|bool|null
     */
    public static function getUserAccess($userId)
    {
        global $db;
        $query = "SELECT `users_caregory`.`name` FROM `users_caregory`
LEFT JOIN users u on users_caregory.id = u.category
WHERE u.id = '$userId';";
        $result = $db -> getRow($query);
        return $result;
    }
}