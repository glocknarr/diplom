<?php
include_once ROOT . '/models/DB/DBModel.php';

class ProductCategory
{
    /**
     * @static  getProductCategoryList
     * Получение списка категорий товаров
     * @return array|bool|null
     */
    public static function getProductCategoryList()
    {
        global $db;
        $productCategoryList    = array(); //Это что за фигня?
        $query                  = "SELECT * FROM `products_category` ORDER BY `sort_order` ASC ";
        $request                = $db->getArray($query);
        return $request;
    }

}