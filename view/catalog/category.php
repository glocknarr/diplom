<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <?php include ROOT . '/view/layouts/catalog.php'; ?>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Последние товары</h2>
                    <?php foreach ($productCategories as $product) : ?>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <a href="product?<?php echo $product['id']; ?>">
                                            <img src="<?php echo $product['picture']; ?>" alt=""/>
                                        </a>
                                        <?php if($product['price'] != 0) : ?>
                                            <h2><?php echo $product['price']; ?></h2>
                                        <? else: ?>
                                            <h2>Образец</h2>
                                        <?php endif; ?>
                                        <p>
                                            <a href="product?<?php echo $product['id']; ?>">
                                                <?php echo $product['name']; ?>
                                            </a>
                                        </p>
                                        <?php if ($product['for_sale'] == 1): ?>
                                            <?php if (User::isGuest()) : ?>
                                                <a href="user?register"
                                                   class="btn btn-default add-to-cart"><i
                                                            class="fa fa-shopping-cart"></i>Купить</a>
                                            <?php else: ?>
                                            <a href="cart?<?php echo $product['id']; ?>"
                                               class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>
                                                <?php if ($product['price'] != 0) : ?>
                                                    Купить</a>
                                                <? else: ?>
                                                    Заказать</a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php if ($product['is_new']) : ?>
                                        <img src="template/images/home/new.png" class="new" alt="">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div><!--features_items-->

                <div style="display: block">
                    <?php echo $pagination -> get(); ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
