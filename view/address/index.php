<?php include ROOT . '/view/layouts/header.php'; ?>

    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Редактирование адресов</h4></div>
                        <div class="panel-body">
                            <div class="box box-info">
                                <div class="box-body">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h4 style="color:#FE980F;"> Адреса доставки </h4>
                                        <table class="table-bordered" style="width: 100%">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>Адрес</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($addresses as $address => $value): ?>
                                                <tr>
                                                    <td><?php echo $value; ?></td>
                                                    <td style="text-align: center"><a href="cabinet?address&edit&<?php echo $address; ?>" class="edit-btn">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                        </a>
                                                    </td>
                                                    <td style="text-align: center"><a href="cabinet?address&delete&<?php echo $address; ?>" class="delete-btn">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        <div style="text-align: center; margin-top: 10px">
                                            <a href="cabinet?address&add"><input type="button" id="addNewAddress"
                                                                                 name="addNewAddress"
                                                                                 class="btn btn-default"
                                                                                 value="Добавить новый адрес"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include ROOT . '/view/layouts/footer.php'; ?>