<?php include ROOT . '/view/layouts/header.php'; ?>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4><?php echo $header; ?></h4></div>
                        <div class="panel-body">
                            <div class="box box-info">
                                <div class="box-body">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h4 style="color:#FE980F;"> Данные адреса: </h4>
                                        <div class="bot-border"></div>
                                        <form action="" method="post" id="addressData">
                                            <div class="address-line">
                                                <label for="new-post-index">Индекс:</label>
                                                <input type="text" name="new-post-index"
                                                    <?php if (isset($addressData['post_index']) && $addressData['post_index'] != 0): ?>
                                                        value="<?php echo $addressData['post_index']; ?>"
                                                    <?php endif; ?>>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="address-line">
                                                <label for="new-city">Город:</label>
                                                <input type="text" name="new-city"
                                                    <?php if (isset($addressData['city'])): ?>
                                                        value="<?php echo $addressData['city']; ?>"
                                                    <?php endif; ?>>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="address-line">
                                                <label for="new-street">Улица:</label>
                                                <input type="text" name="new-street"
                                                    <?php if (isset($addressData['street'])): ?>
                                                        value="<?php echo $addressData['street']; ?>"
                                                    <?php endif; ?>>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="address-line">
                                                <label for="new-house">Дом:</label>
                                                <input type="text" name="new-house"
                                                    <?php if (isset($addressData['house']) && $addressData['house'] != 0): ?>
                                                        value="<?php echo $addressData['house']; ?>"
                                                    <?php endif; ?>>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="address-line">
                                                <label for="new-corps">Корпус:</label>
                                                <input type="text" name="new-corps"
                                                    <?php if (isset($addressData['corps'])): ?>
                                                        value="<?php echo $addressData['corps']; ?>"
                                                    <?php endif; ?>>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="address-line">
                                                <label for="new-apartment">Квартира:</label>
                                                <input type="text" name="new-apartment"
                                                    <?php if (isset($addressData['apartment']) && $addressData['apartment'] != 0): ?>
                                                        value="<?php echo $addressData['apartment']; ?>"
                                                    <?php endif; ?>>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="address-line">
                                                <label for="new-other-info">Дополнительно:</label>
                                                <textarea name="new-other-info"><?php if (isset($addressData['other_info'])): ?><?php echo $addressData['other_info']; ?><?php endif; ?></textarea>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div style="text-align: center">
                                                <input type="submit" id="submit" name="submit"
                                                       class="btn btn-default address-btn"
                                                       style="margin-top: 5px" form="addressData"
                                                />
                                            </div>
                                        </form>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include ROOT . '/view/layouts/footer.php'; ?>