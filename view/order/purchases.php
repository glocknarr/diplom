<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <?php if ($purchases): ?>
                        <?php foreach ($purchases as $purchase): ?>
                            <div class="purchase">
                                <img src="<?php echo $purchase['picture']; ?>" alt="Изображение товара">
                                <div class="purchase_head">
                                    <div class="purchase_info">
                                        <div class="productinfo">
                                            <h3><?php echo $purchase['product_name']; ?></h3>
                                            <h4><?php echo $purchase['price']; ?> руб.
                                                <span class="status">(Статус: <?php echo $purchase['order_status']; ?>)</span>
                                            </h4>
                                            <a href="shop?<?php echo $purchase['shop_id']; ?>">
                                                <h4><?php echo $purchase['shop_name'] ?></h4>
                                            </a>
                                            <div><?php echo $purchase['order_date']; ?></div>
                                        </div>
                                    </div>
                                    <div class="price">

                                    </div>
                                </div>
                                <div class="purchase_desc"><?php echo $purchase['product_description']; ?></div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="panel-heading">
                            <h4>Пока небыло ни одной покупки.</h4>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/view/layouts/footer.php'; ?>
