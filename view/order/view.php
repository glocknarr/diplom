<?php include ROOT . '/view/layouts/header.php'; ?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Информация по заказу</h4></div>
                    <div class="panel-body">
                        <div class="box box-info">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div align="center" class="view-product">
                                        <img src="<?php echo $fullOrderInfo[0]['picture']; ?>" alt="Изображение товара">
                                    </div>
                                    <br>
                                </div>
                                <div class="col-sm-6">
                                    <h4 style="color:#FE980F;"> <?php echo $fullOrderInfo[0]['product_info']['name']; ?> </h4>
                                    <div class="bot-border"></div>
                                    <ul>
                                        <li><?php if ($fullOrderInfo[0]['product_info']['price'] != 0) : ?>
                                                <h2><?php echo $fullOrderInfo[0]['product_info']['price'] . ' руб.'; ?></h2>
                                            <? else: ?>
                                                <?php if ($fullOrderInfo[0]['product_info']['for_sale'] != 1): ?>
                                                    <h2>Не доступно для заказа</h2>
                                                <?php else: ?>
                                                    <h2>Образец</h2>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </li>
                                        <li><?php if ($fullOrderInfo[0]['product_info']['payment_info'] != '') : ?>
                                                <div class="title">Оплата:</div>
                                                <div
                                                        class="product_description"> <?php echo $fullOrderInfo[0]['product_info']['payment_info']; ?></div>
                                            <?php endif; ?>
                                        </li>
                                        <li><?php if ($fullOrderInfo[0]['product_info']['delivery_info'] != '') : ?>
                                                <div class="title">Доставка:</div>
                                                <div
                                                        class="product_description"><?php echo $fullOrderInfo[0]['product_info']['delivery_info']; ?></div>
                                            <?php endif; ?>
                                        </li>
                                        <li><?php if ($fullOrderInfo[0]['product_info']['description'] != '') : ?>
                                                <div class="title">Описание:</div>
                                                <div
                                                        class="product_description"><?php echo $fullOrderInfo[0]['product_info']['description']; ?></div>
                                            <?php endif; ?>
                                        </li>
                                        <div class="bot-border"></div>
                                        <li><?php if ($fullOrderInfo[0]['status'] != '') : ?>
                                                <div class="title">Статус:</div>
                                                <div
                                                        class="product_description"><?php echo $fullOrderInfo[0]['status']; ?></div>
                                            <?php endif; ?>
                                        </li>
                                        <li><?php if ($fullOrderInfo[0]['order_date'] != '') : ?>
                                                <div class="title">Дата заказа:</div>
                                                <div
                                                        class="product_description"><?php echo $fullOrderInfo[0]['order_date']; ?></div>
                                            <?php endif; ?>
                                        </li>
                                        <li><?php if ($fullOrderInfo[0]['delivery_date'] != '0000-00-00') : ?>
                                                <div class="title">Дата закрытия заказа:</div>
                                                <div
                                                        class="product_description"><?php echo $fullOrderInfo[0]['delivery_date']; ?></div>
                                            <?php endif; ?>
                                        </li>
                                        <div class="bot-border"></div>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                                <hr style="margin:5px 0 5px 0;">

                                <?php $customer = $fullOrderInfo[0]['customer_info'] ?>

                                <div class="col-sm-5 col-xs-6 title ">Имя:</div>
                                <div class="col-sm-7 col-xs-6 ">
                                    <p class="label-width" id="order_name">
                                        <?php echo $customer['name']; ?>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 title ">Фамилия:</div>
                                <div class="col-sm-7">
                                    <p class="label-width" id="order_surname">
                                        <?php echo $customer['surname']; ?>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 title ">Отчество:</div>
                                <div class="col-sm-7">
                                    <p class="label-width" id="order_patronymic">
                                        <?php echo $customer['patronymic']; ?>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 title ">Email:</div>
                                <div class="col-sm-7">
                                    <p class="label-width" id="order_email">
                                        <?php echo $customer['email']; ?>
                                    </p>
                                </div>

                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 title ">Телефон:</div>
                                <div class="col-sm-7">
                                    <p class="label-width" id="order_phone">
                                        <?php echo $customer['phone']; ?>
                                    </p>
                                </div>

                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 title ">Адрес доставки:</div>
                                <div class="col-sm-7">
                                    <p class="label-width" id="order_phone">
                                        <?php echo $customer['address']; ?>
                                    </p>
                                </div>

                                <div class="clearfix"></div>
                                <div class="bot-border"></div>

                                <div class="col-sm-5 col-xs-6 title ">Дополнительная информация:</div>
                                <div class="col-sm-7">
                                    <p class="label-width" id="order_description">
                                        <?php echo $fullOrderInfo[0]['description']; ?>
                                    </p>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
