<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <?php if ($orderList): ?>
                        <?php foreach ($orderList as $order => $item): ?>
                            <a href="orders?<?php echo $order; ?>">
                                <div class="order_small <?php if ($item['status'] == 'Новый'): ?> order_new <? endif; ?>">
                                    <img src="<?php echo $item['picture']; ?>" alt="Изображение товара">
                                    <div class="order_small_info">
                                        <div class="productinfo">
                                            <h2><?php echo $item['product_info']['name'] ?></h2>
                                            <?php echo $item['order_date'] ?>
                                        </div>

                                    </div>
                                    <div class="productinfo price">
                                        <?php if ($item['price'] == 0): ?>
                                            <h2>Образец</h2>
                                        <?php else: ?>
                                            <h2><?php echo $item['price'] ?> руб.</h2>
                                        <?php endif; ?>
                                        <div class="status"><?php echo $item['status']; ?></div>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <div class="panel-heading">
                        <h4>К сожалению, заказов, пока, нет.</h4>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
