<?php include ROOT . '/view/layouts/admin_header.php'; ?>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Информация по аккаунтам: </h4></div>
                        <div class="panel-body table-style">
                            <table class="table-bordered" style="width: 100%">
                                <thead>
                                <tr class="thead-light">
                                    <th>ID</th>
                                    <th>ФИО</th>
                                    <th>email</th>
                                    <th>Зарегистрирован</th>
                                    <th>Телефон</th>
                                    <th>WEB</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($accountsInfo as $account): ?>
                                    <tr>
                                        <td><?php echo $account['id']; ?></td>
                                        <td><?php echo $account['fio']; ?></td>
                                        <td><?php echo $account['email']; ?></td>
                                        <td><?php echo $account['registration']; ?></td>
                                        <td><?php echo $account['phone']; ?></td>
                                        <td><?php echo $account['web']; ?></td>
                                        <td>
                                            <a href="cabinet?edit&<?php echo $account['id']; ?>">
                                                <span class="glyphicon glyphicon-pencil" style="color: green"></span>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include ROOT . '/view/layouts/admin_footer.php'; ?>