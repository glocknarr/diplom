<?php include ROOT . '/view/layouts/admin_header.php'; ?>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Информация по товарам: </h4></div>
                        <div class="panel-body table-style">
                            <table class="table-bordered" style="width: 100%">
                                <thead>
                                <tr class="thead-light">
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Цена</th>
                                    <th>Магазин</th>
                                    <th>Продается?</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($productInfo as $product): ?>
                                    <tr>
                                        <td><?php echo $product['id']; ?></td>
                                        <td><?php echo $product['name']; ?></td>
                                        <td><?php echo $product['price']; ?></td>
                                        <td><?php echo $product['shop']; ?></td>
                                        <td><?php echo $product['for_sale']; ?></td>
                                        <td>
                                            <a href="product?edit&<?php echo $product['id']; ?>">
                                                <span class="glyphicon glyphicon-pencil" style="color: green"></span>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include ROOT . '/view/layouts/admin_footer.php'; ?>