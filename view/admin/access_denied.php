<?php include ROOT . '/view/layouts/header.php'; ?>

<section class="content">
    <div class="container">
        <div class="row">
            <p class="title">
               Вход в панель управления доступен только администратору!
            </p>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
