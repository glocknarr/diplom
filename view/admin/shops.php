<?php include ROOT . '/view/layouts/admin_header.php'; ?>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Информация по магазинам: </h4></div>
                        <div class="panel-body table-style">
                            <table class="table-bordered" style="width: 100%">
                                <thead>
                                <tr class="thead-light">
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Владелец</th>
                                    <th>Зарегистрирован</th>
                                    <th>Описание</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($shopsInfo as $shop): ?>
                                    <tr>
                                        <td><?php echo $shop['id']; ?></td>
                                        <td><?php echo $shop['name']; ?></td>
                                        <td><?php echo $shop['fio']; ?></td>
                                        <td><?php echo $shop['registration']; ?></td>
                                        <td><?php echo $shop['description']; ?></td>
                                        <td>
                                            <a href="shop?configure&<?php echo $shop['id']; ?>">
                                                <span class="glyphicon glyphicon-pencil" style="color: green"></span>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include ROOT . '/view/layouts/admin_footer.php'; ?>