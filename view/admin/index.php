<?php include ROOT . '/view/layouts/admin_header.php'; ?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Общая статистика: </h4></div>
                    <div class="panel-body">
                        <p>Количество пользователей: <span class="title"><?php echo $siteInfo['countUsers']; ?></span></p>
                        <p>Количество магазинов: <span class="title"><?php echo $siteInfo['countShops']; ?></span></p>
                        <p>Количество товаров: <span class="title"><?php echo $siteInfo['countProducts']; ?></span></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/admin_footer.php'; ?>
