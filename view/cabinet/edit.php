<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-offset-1">
                <?php if ($result): ?>
                    <p>Данные отредактированы</p>
                <?php else : ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Редактирование данных пользователя</h4></div>
                        <div class="panel-body">
                            <form action="#" method="post">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <div class="clearfix"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Имя:</div>
                                        <div class="col-sm-7 col-xs-6 "><input type=" text" name="name"
                                                                               value='<?php echo $name; ?>'
                                                                               placeholder="Имя" class="label-width"/>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Фамилия:</div>
                                        <div class="col-sm-7"><input class="label-width" type="text" name="surname"
                                                                     placeholder="Фамилия"
                                                                     value="<?php echo $surname; ?>"/></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Отчество:</div>
                                        <div class="col-sm-7"><input class="label-width" type="text" name="patronymic"
                                                                     placeholder="Отчество"
                                                                     value="<?php echo $patronymic; ?>"/></div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Пароль:</div>
                                        <div class="col-sm-7"><input class="label-width" type="password" name="password"
                                                                     placeholder="Пароль"
                                                                     value="<?php echo $password; ?>"/></div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">День рождения:</div>
                                        <div class="col-sm-7"><input class="label-width" type="date" name="birthday"
                                                                     placeholder="<?php echo User ::getBirthDay($user['birthday']); ?>"
                                                                     value="<?php echo $birthday; ?>"/>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Email:</div>
                                        <div class="col-sm-7"><input class="label-width" type="email" name="email"
                                                                     placeholder="E-mail"
                                                                     value="<?php echo $email; ?>"/></div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Телефон:</div>
                                        <div class="col-sm-7"><input class="label-width" type="text" name="phone"
                                                                     placeholder="Телефон"
                                                                     value="<?php echo $phone; ?>"/></div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Сайт:</div>
                                        <div class="col-sm-7"><input class="label-width" type="text" name="web"
                                                                     placeholder="Адрес страницы"
                                                                     value="<?php echo $web; ?>"/></div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>
                                        <div class="col-sm-5 col-xs-6 title ">Адреса доставки:</div>
                                        <div class="col-md-7 col-sm-7 col-xs-12" style="display: inline-block">
                                            <ol>
                                                <?php foreach ($address as $addr => $value): ?>
                                                    <?php echo "<li>"; ?>
                                                    <div>
                                                        <?php echo $value; ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <?php echo "</li> "; ?>
                                                <?php endforeach; ?>
                                            </ol>
                                        </div>
                                    </div>

                                </div>
                        </div>
                        <div align="center">
                            <div class="bot-border"></div>
                            <input type="submit" name="submit" class="btn btn-default" value="Сохранить"/>
                        </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
