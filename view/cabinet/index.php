<?php include ROOT . '/view/layouts/header.php'; ?>
    <section class="content" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-offset-2">
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Кабинет пользователя</h4></div>
                        <div class="panel-body">
                            <div class="box box-info">
                                <div class="box-body">
                                    <div class="col-sm-6">
                                        <div align="center">
                                            <img alt="User Pic"
                                                 src="<?php echo $avatar; ?>"
                                                 id="profile-image" class="img-circle img-responsive">
                                            <form action="avatar" method="post" enctype="multipart/form-data">
                                                <label for="avatar"
                                                       style="color:#999; cursor: pointer; text-decoration: underline">
                                                    Выбрать изображение
                                                </label>
                                                <input id="avatar" class="hidden" type="file" name="avatar">
                                                <input type="submit" name="upload_button" class="upload_button">
                                            </form>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4 style="color:#FE980F;"> <?php echo $user['name']; ?> </h4>
                                        <div class="bot-border"></div>
                                        <ul>
                                            <li><a href="cabinet?edit"><i class="fa fa-pencil"></i> Редактировать данные</a>
                                            </li>
                                            <li><a href="purchases"><i class="fa fa-shopping-cart"></i> Список
                                                    покупок</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr style="margin:5px 0 5px 0;">

                                    <div class="col-sm-5 col-xs-6 title ">Имя:</div>
                                    <div class="col-sm-7 col-xs-6 "><?php echo $user['name']; ?></div>
                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Фамилия:</div>
                                    <div class="col-sm-7"> <?php echo $user['surname']; ?></div>
                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Отчество:</div>
                                    <div class="col-sm-7"> <?php echo $user['patronymic']; ?></div>
                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Дата регистрации:</div>
                                    <div class="col-sm-7"><?php echo $user['registration']; ?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">День рождения:</div>
                                    <div class="col-sm-7"><?php echo User ::getBirthDay($user['birthday']); ?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>


                                    <div class="col-sm-5 col-xs-6 title ">Email:</div>
                                    <div class="col-sm-7"><?php echo $user['email']; ?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Телефон:</div>
                                    <div class="col-sm-7"><?php echo $user['phone']; ?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Сайт:</div>
                                    <div class="col-sm-7"><?php echo $user['web']; ?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Адреса доставки:</div>
                                    <div class="col-sm-7">
                                        <ol>
                                            <?php foreach ($address as $addr): ?>
                                                <?php echo "<li>" . $addr . "</li> "; ?>
                                            <?php endforeach; ?>
                                        </ol>
                                        <a href="cabinet?address"><input type="button" id="addAddress" name="addAddress"
                                               class="btn btn-default address-btn"
                                               value="Редактировать адреса"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/view/layouts/footer.php'; ?>