<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Интернет-магазин товаров ручной работы">
    <meta name="author" content="">
    <title>Магазин товаров ручной работы</title>
    <link href="template/css/bootstrap.min.css" rel="stylesheet">
    <link href="template/css/font-awesome.min.css" rel="stylesheet">
    <link href="template/css/prettyPhoto.css" rel="stylesheet">
    <link href="template/css/animate.css" rel="stylesheet">
    <link href="template/css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="template/images/ico/favicon.png">
</head>
<body>
<header id="header">
    <div class="header-middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="index">
                            <img src="template/images/home/mobile_logo.jpg" class="mobile_logo" alt="Logo"/>
                            <img src="template/images/home/logo.png" class="my-logo" alt="Logo"/>
                        </a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <?php if (User ::isGuest()): ?>
                                <li><a href="user?login"><i class="glyphicon glyphicon-log-in"></i> Вход</a></li>
                                <li><a href="user?register"><i class="glyphicon glyphicon-user"></i> Регистрация</a>
                                </li>
                            <?php else: ?>
                                <li><a href="orders">
                                        <div class="mobile_invisible"><i class="fa fa-envelope"></i>Заказы</div>
                                    </a></li>
                                <li style="padding-right: 0; margin-right: 0"><a
                                            href="cabinet"><?php if ($_SESSION['user']): ?>
                                            <?php $extentions = ['jpg', 'jpeg', 'png']; ?>
                                            <?php foreach ($extentions as $ext): ?>
                                                <?php if (file_exists('userfiles/' . $_SESSION['user'] . '/avatar.' . $ext)): ?>
                                                    <?php $avatar = 'userfiles/' . $_SESSION['user'] . '/avatar.' . $ext; ?>
                                                    <?php break; ?>
                                                <?php else: ?>
                                                    <?php $avatar = DEFAULT_AVATAR; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <?php $avatar = DEFAULT_AVATAR; ?>
                                        <?php endif; ?>
                                        <img alt="User Pic"
                                             src="<?php echo $avatar; ?>"
                                             id="avatar_top" class="img-circle "></a>
                                </li>
                                <li style="padding-left: 5px"><a href="cabinet">
                                        <div>
                                            <?php if (isset((User ::getUserById($_SESSION['user']))['name'])) : ?>
                                                <?php echo (User ::getUserById($_SESSION['user']))['name']; ?>
                                            <?php else: ?>
                                                Аккаунт
                                            <?php endif; ?>
                                        </div>
                                    </a></li>
                                <li><a href="user?logout"><i class="glyphicon glyphicon-log-out"></i> Выход</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Компактное меню</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left mobile_align">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="index">Главная</a></li>
                            <li class="dropdown"><a href="#">Магазин<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="shop">Мой магазин</a></li>
                                    <li><a href="orders">Мои заказы</a></li>
                                </ul>
                            </li>
                            <li><a href="purchases">Мои покупки</a></li>
                            <li><a href="about">О нас</a></li>
                            <li><a href="contacts">Контакты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->