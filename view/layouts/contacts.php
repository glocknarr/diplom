<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Контактная информация</h4></div>
                    <div class="panel-body">
                        Ваши пожелания, предложения и вопросы Вы можете направлять по адресу:
                        <div style="text-align: center"><a type="mailto">admin@craft_shop.ru</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
