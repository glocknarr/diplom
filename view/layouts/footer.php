<footer id="footer">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2019</p>
                <p class="pull-right">Craft shop</p>
            </div>
        </div>
    </div>
</footer>
</body>
<script src="template/js/jquery.js"></script>
<script src="template/js/bootstrap.min.js"></script>
<script src="template/js/jquery.scrollUp.min.js"></script>
<script src="template/js/jquery.prettyPhoto.js"></script>
<script src="template/js/main.js"></script>
</html>