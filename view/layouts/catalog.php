<div class="col-sm-3 mobile-category">
    <div class="left-sidebar">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-mobile">
                <h2>Каталог</h2>
            </button>
        </div>
        <div class="mainmenu pull-left">
            <ul class="nav navbar-nav collapse navbar-mobile">
                <?php foreach ($categories as $categoryItem) : ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <li><a href="category?<?php echo $categoryItem['id']; ?>">
                                        <?php echo $categoryItem['name']; ?>
                                    </a>
                                </li>
                            </h4>
                        </div>
                    </div>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-3 big-category">
    <div class="left-sidebar">
        <a href="index"><h2>Каталог</h2></a>
        <div class="panel-group category-products">

            <?php foreach ($categories as $categoryItem) : ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="category?<?php echo $categoryItem['id']; ?>">
                                <?php echo $categoryItem['name']; ?>
                            </a>
                        </h4>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
