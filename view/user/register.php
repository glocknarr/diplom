<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">

            <div class="col-sm-8 col-sm-offset-2 padding-right">
                <?php if ($result): ?>
                    <p>Вы зарегистрированы!</p>
                <?php else : ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <div class="signup-form"><!--sign up form-->
                        <h2>Регистрация на сайте</h2>
                        <form action="#" method="post">
                            <div>
                                <div class="red-field">*</div>
                                <label for="name">Имя:</label>
                                <input type="text" name="name" value='<?php echo $name; ?>'
                                       placeholder="Имя"/>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <div class="red-field">*</div>
                                <label for="login">Логин:</label>
                                <input type="text" name="login" placeholder="login" value="<?php echo $login; ?>"/>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <div class="red-field">*</div>
                                <label for="password">Пароль:</label>
                                <input type="password" name="password" placeholder="Пароль" value=""/>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <div class="red-field">*</div>
                                <label for="password2">Повторите пароль:</label>
                                <input type="password" name="password2" placeholder="Повторить пароль" value=""/>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <div class="red-field">*</div>
                                <label for="email">email:</label>
                                <input type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                            </div>
                            <div class="clearfix"></div>
                            <div class="captcha">
                                <div class="captcha_pict">
                                    <?php echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code">'; ?>
                                </div>
                                <div class="captcha-input">
                                    <div class="red-field">*</div>
                                    <label for="user_captcha">Введите символы изображенные на картинке: </label>
                                    <input type="text" name="user_captcha">
                                    <input type="text" name="obr" style="display: none" value="<?php echo $_SESSION['captcha']['code']; ?>" >

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-check">
                                <label>
                                    <input type="checkbox" name="check"> <span class="label-text">Даю согласие на обработку и хранение персональных данных</span>
                                </label>
                            </div>
                            <div class="clearfix"></div>

                            <a href="about">Пользовательское соглашение</a>
                            <input type="submit" name="submit" class="btn btn-default btn-register"
                                   value="Регистрация"/>
                        </form>
                    </div><!--/sign up form-->
                <?php endif; ?>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
