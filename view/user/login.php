<?php include ROOT . '/view/layouts/header.php'; ?>

    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 padding-right">
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <div class="signup-form"><!--sign up form-->
                        <h2>Вход на сайт</h2>
                        <form action="#" method="post">
                            <div>
                                <label for="login">Логин: </label>
                                <input type="text" name="login" placeholder="Логин" value="<?php echo $login; ?>"/>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <label for="password">Пароль:</label>
                                <input type="password" name="password" placeholder="Пароль"
                                   value="<?php echo $password; ?>"/>
                            </div>
                            <input type="submit" name="submit" class="btn btn-default" value="Вход"/>
                        </form>
                        <a href="user?register">Зарегистрироваться</a>
                    </div><!--/sign up form-->


                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/view/layouts/footer.php'; ?>