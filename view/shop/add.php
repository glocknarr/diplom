<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Панель управления</h2>
                    <div class="panel-group category-products">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="shop?configure">Настройка магазина</a>
                                    <a href="shop?add">Добавить товар</a><br>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Добавление нового товара</h4></div>
                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-4 col-sm-6 col-xs-8 product-picture">
                                    <div class="view-product">
                                        <img alt="Product Pic"
                                             src="<?php echo DEFAULT_PICTURES; ?>"
                                             id="product-image" class="img-responsive">
                                        <form action="productPicture" method="post" enctype="multipart/form-data">
                                            <label for="product-picture" style="color:#999; cursor: pointer; text-decoration: underline">
                                                Выбрать изображение
                                            </label>
                                            <input id="product-picture" class="hidden" type="file" name="product-picture">
                                            <input type="submit" name="upload_picture" class="upload_button">
                                        </form>
                                        <?php echo $tmp_pict ?>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <form action="#" method="post">
                                        <div class="box box-info">
                                            <div class="box-body">
                                                <div class="clearfix"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Наименование:</div>
                                                <div class="col-sm-7 col-xs-6 "><input type="text" name="name"
                                                                                       placeholder="Наименование товара"
                                                                                       class="label-width"/>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Стоимость:</div>
                                                <div class="col-sm-7 col-xs-6"><input class="label-width" type="text"
                                                                             name="price"
                                                                             placeholder="Стоимость"/></div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>
                                                <div class="clearfix"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Категория:</div>
                                                <div class="col-sm-7">
                                                    <select name="category" id="category" value="">
                                                        <?php foreach ($productCategory as $cat): ?>
                                                            <?php echo "<option value='" . $cat['id'] . "'>" . $cat['name'] . "</option>"; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Описание товара:</div>
                                                <div class="col-sm-7"><textarea rows="4" class="label-width"
                                                                                name="description"
                                                                                placeholder="Описание товара"></textarea>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Оплата:</div>
                                                <div class="col-sm-7"><textarea rows="4" class="label-width"
                                                                                name="payment_info"
                                                                                placeholder="Как оплачивать"></textarea>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Доставка:</div>
                                                <div class="col-sm-7"><textarea rows="4" class="label-width"
                                                                                name="delivery_info"
                                                                                placeholder="Описание вариантов доставки"></textarea>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Это новый</div>
                                                <div class="form-check">
                                                    <label>
                                                        <input type="checkbox" name="is_new" checked>
                                                        <span class="label-text"></span>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Продается</div>
                                                <div class="form-check">
                                                    <label>
                                                        <input type="checkbox" name="for_sale" checked> <span
                                                                class="label-text"></span>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>

                                                <div class="col-sm-5 col-xs-6 title ">Показывать на витрине
                                                </div>
                                                <div class="form-check">
                                                    <label>
                                                        <input type="checkbox" name="visible" checked> <span
                                                                class="label-text"></span>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="bot-border"></div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div align="center">
                                <input type="submit" name="submit" class="btn btn-default" value="Сохранить"/>
                            </div>
                            </form>
                        </div>
                    </div><!--features_items-->
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php include ROOT . '/view/layouts/footer.php'; ?>
