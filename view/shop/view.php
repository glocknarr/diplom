<?php include ROOT . '/view/layouts/header.php'; ?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Каталог</h2>
                    <div class="panel-group category-products">
                        <?php foreach ($categories as $categoryItem) : ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="category?<?php echo $categoryItem['id']; ?>">
                                            <?php echo $categoryItem['name']; ?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <div>
                        <h2 class="title text-center"><?php echo $shopName['name']; ?></h2>
                        <div class="col-sm-9">
                            <?php echo $shopName['description']; ?><br>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php foreach ($shopProduct as $product) : ?>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <a href="product?<?php echo $product['id']; ?>">
                                            <img src="<?php echo $product['picture']; ?>"
                                                 alt="<?php echo $product['name']; ?>"/>
                                        </a>
                                        <?php if ($product['price'] != 0 && $product['for_sale'] == 1) : ?>
                                            <h2><?php echo $product['price']; ?></h2>
                                        <? else: ?>
                                            <h2>Образец</h2>
                                        <?php endif; ?>
                                        <p>
                                            <a href="product?<?php echo $product['id']; ?>">
                                                <?php echo $product['name']; ?>
                                            </a>
                                        </p>
                                        <?php if ($product['for_sale'] == 1): ?>
                                            <?php if (User::isGuest()) : ?>
                                                <a href="user?login"
                                                   class="btn btn-default add-to-cart"><i
                                                            class="fa fa-shopping-cart"></i>Купить</a>
                                            <?php else: ?>
                                            <a href="cart?<?php echo $product['id']; ?>"
                                               class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>
                                                <?php if ($product['price'] != 0 && $product['for_sale'] == 1) : ?>
                                                    Купить</a>
                                                <? else: ?>
                                                    Заказать</a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php if ($product['is_new']) : ?>
                                        <img src="template/images/home/new.png" class="new" alt="">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/view/layouts/footer.php'; ?>
