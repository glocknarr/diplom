<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Панель управления</h2>
                    <div class="panel-group category-products">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <?php if ($shop == false): ?>
                                        <a href="shop?create">Создать магазин</a>
                                    <?php else: ?>
                                        <a href="shop?configure">Настройка магазина</a>
                                        <a href="shop?add">Добавить товар</a><br>
                                    <?php endif; ?>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <?php if (Shop::checkShop()): ?>
                        <div>
                            <h2 class="title text-center"><?php echo $dataShop['name']; ?></h2>
                            <div class="col-sm-9">
                                <?php echo $dataShop['description']; ?><br>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <h2 class="title text-center">Мои работы</h2>
                        <?php if (!empty($myProducts)): ?>
                            <?php foreach ($myProducts as $product) : ?>
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="<?php echo $product['picture']; ?>" alt=""/>
                                                <?php if($product['price'] != 0) : ?>
                                                    <h2><?php echo $product['price']; ?></h2>
                                                <? else: ?>
                                                    <h2>Образец</h2>
                                                <?php endif; ?>
                                                <p>
                                                    <a href="product?<?php echo $product['id']; ?>">
                                                        <?php echo $product['name']; ?>
                                                    </a>
                                                </p>
                                                <a href="product?edit&<?php echo $product['id']; ?>"
                                                   class="btn btn-default add-to-cart">
                                                    <i class="fa fa-pencil"></i>Редактировать</a>
                                                <a href="product?delete&<?php echo $product['id']; ?>"
                                                   class="btn btn-default add-to-cart"
                                                   onClick="return window.confirm('Удалить выбранный товар?')">
                                                    <i class="fa fa-trash-o"></i>Удалить</a>
                                            </div>
                                            <?php if ($product['is_new']) : ?>
                                                <img src="template/images/home/new.png" class="new" alt="">
                                            <?php endif; ?>
                                            <?php if (!$product['visible']) : ?>
                                                <img src="template/images/home/not_visible.png" class="new" alt="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <?php echo "<div align='center'><h4>В магазине нет товаров.</h4><br><a href='shop?add' class='btn btn-default'>Добавить</a></div>"; ?>
                        <?php endif; ?>
                    <?php else : ?>
                        <?php echo "<div align='center'><h4>Магазин еще не создан.</h4><br><a href='shop?create' class='btn btn-default'>Создать</a></div>"; ?>

                    <?php endif; ?>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/view/layouts/footer.php'; ?>
