<?php include ROOT . '/view/layouts/header.php'; ?>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-offset-2">
                <?php if (isset($errors) && is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li> - <?php echo $error; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Создание магазина</h4></div>
                    <div class="panel-body">
                        <form action="#" method="post">
                            <div class="box box-info">
                                <div class="box-body">

                                    <div class="clearfix"></div>
                                    <div class="col-sm-5 col-xs-6 title ">Наименование:</div>
                                    <div class="col-sm-7 col-xs-6 "><input type="text" name="name"
                                                                           placeholder="Название магазина"
                                                                           class="label-width"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 title ">Описание:</div>
                                    <div class="col-sm-7">
                                        <textarea rows="5" class="label-width" name="description"
                                                  placeholder="Описание магазина"></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div align="center">
                                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/view/layouts/footer.php'; ?>
