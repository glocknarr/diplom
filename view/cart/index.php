<?php include ROOT . '/view/layouts/header.php'; ?>

    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Оформление заказа</h4></div>
                        <div class="panel-body">
                            <form action="#" method="post">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <div class="col-sm-6">
                                            <div align="center" class="view-product">
                                                <img src="<?php echo $productInfo[0]['picture']; ?>"
                                                     alt="Изображение товара">
                                            </div>
                                            <br>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 style="color:#FE980F;"> <?php echo $productInfo[0]['name']; ?> </h4>
                                            <div class="bot-border"></div>
                                            <ul>
                                                <li><?php if ($productInfo[0]['price'] != 0) : ?>
                                                        <h2><?php echo $productInfo[0]['price'] . ' руб.'; ?></h2>
                                                    <? else: ?>
                                                        <?php if ($productInfo[0]['for_sale'] != 1): ?>
                                                            <h2>Не доступно для заказа</h2>
                                                        <?php else: ?>
                                                            <h2>Образец</h2>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </li>
                                                <li><?php if ($productInfo[0]['payment_info'] != '') : ?>
                                                        <div class="title">Оплата:</div>
                                                        <div class="product_description"> <?php echo $productInfo[0]['payment_info']; ?></div>
                                                    <?php endif; ?>
                                                </li>
                                                <li><?php if ($productInfo[0]['delivery_info'] != '') : ?>
                                                        <div class="title">Доставка:</div>
                                                        <div class="product_description"><?php echo $productInfo[0]['delivery_info']; ?></div>
                                                    <?php endif; ?>
                                                </li>
                                                <li><?php if ($productInfo[0]['description'] != '') : ?>
                                                        <div class="title">Описание:</div>
                                                        <div class="product_description"><?php echo $productInfo[0]['description']; ?></div>
                                                    <?php endif; ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr style="margin:5px 0 5px 0;">

                                        <?php $customer = Cart ::getCustomerInfo($_SESSION['user']); ?>
                                        <div class="col-sm-5 col-xs-6 title ">Имя:</div>
                                        <div class="col-sm-7 col-xs-6 ">
                                            <input class="label-width" id="order_name" name="order_name" type="text"
                                                   value="<?php echo $customer['name']; ?>">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Фамилия:</div>
                                        <div class="col-sm-7 col-xs-6">
                                            <input class="label-width" id="order_surname" name="order_surname"
                                                   type="text"
                                                   value="<?php echo $customer['surname']; ?>">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Отчество:</div>
                                        <div class="col-sm-7 col-xs-6">
                                            <input class="label-width" id="order_patronymic" name="order_patronymic"
                                                   type="text"
                                                   value="<?php echo $customer['patronymic']; ?>">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Email:</div>
                                        <div class="col-sm-7 col-xs-6">
                                            <input class="label-width" id="order_email" name="order_email" type="text"
                                                   value="<?php echo $customer['email']; ?>">
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Телефон:</div>
                                        <div class="col-sm-7 col-xs-6">
                                            <input class="label-width" id="order_phone" name="order_phone" type="text"
                                                   value="<?php echo $customer['phone']; ?>">
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-6 title ">Адрес доставки:</div>
                                        <div class="col-sm-7">
                                            <select class="label-width" name="address" id="order_address">
                                                <?php foreach ($customer['address'] as $addr): ?>
                                                    <?php echo "<option>" . $addr . "</option> "; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="bot-border"></div>

                                        <div class="col-sm-5 col-xs-12 title ">Дополнительная информация:</div>
                                        <div class="col-sm-7">
                                            <textarea class="label-width" id="order_description"
                                                      name="order_description"></textarea>
                                        </div>
                                        <?php if ($productInfo[0]['for_sale'] == 1): ?>
                                            <div align="center">
                                                <input type="hidden" id="productId" name="product_Id"
                                                       value="<?php echo $productInfo[0]['id']; ?>">
                                                <input type="hidden" id="customerId" name="customer_Id"
                                                       value="<?php echo $customer['id']; ?>">
                                                <input type="submit" name="submit" class="btn btn-default"
                                                       value="Заказать"/>
                                            </div>
                                        <? endif; ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>


<?php include ROOT . '/view/layouts/footer.php'; ?>