<?php include ROOT . '/view/layouts/header.php'; ?>
    <section class="content">
        <div class="container">
            <div class="row">
                <?php include ROOT . '/view/layouts/catalog.php'; ?>

                <div class="col-sm-9 padding-right">
                    <div class="product-details"><!--product-details-->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="view-product">
                                    <img src="<?php echo $product['picture']; ?>" alt=""/>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="product-information"><!--/product-information-->
                                    <?php if ($product['is_new'] == 1): ?>
                                        <img src="template/images/product-details/new.jpg" class="newarrival" alt=""/>
                                    <?php endif; ?>
                                    <h2><?php echo $product['name']; ?></h2>
                                    <h3>
                                        <a href="shop?<?php echo $shopName['id']; ?>"><?php echo $shopName['name']; ?></a>
                                    </h3>
                                    <span>
                                            <?php if ($product['price'] != 0 && $product['for_sale'] == 1): ?>
                                                <span>Цена: <?php echo $product['price']; ?> руб.</span>
                                            <?php else: ?>
                                                <span>Образец</span>
                                            <?php endif ?>
                                             <a href=<?php echo "cart?" . $product['id']; ?>>
                                                 <?php if ($product['for_sale'] == 1): ?>
                                                     <button type="button" class="btn btn-default cart">
                                                     <i class="fa fa-shopping-cart"></i>
                                                     <?php if ($product['price'] != 0) : ?>
                                                         Купить
                                                     <?php else: ?>
                                                         Заказать
                                                     <?php endif ?>
                                                    </button>
                                                 <?php endif; ?>
                                             </a>
                                        </span>
                                </div><!--/product-information-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Описание товара</h5>
                                <p class="product_description"><?php echo $product['description']; ?></p>
                                <?php if ($product['payment_info'] != ''): ?>
                                    <h5>Оплата</h5>
                                    <p class="product_description"><?php echo $product['payment_info']; ?></p>
                                <?php endif; ?>
                                <?php if ($product['payment_info'] != ''): ?>
                                    <h5>Доставка</h5>
                                    <p class="product_description"><?php echo $product['delivery_info']; ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div><!--/product-details-->
                </div>
            </div>
        </div>
    </section>
    <br/>
    <br/>
<?php include ROOT . '/view/layouts/footer.php'; ?>